﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using CompanyParser.Parsers;
using System.Linq;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using CompanyParser.IO;
using CefSharp;
using CefSharp.WinForms;
using CompanyParser.data;

namespace CompanyParser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            InitializeWb();

            this.panel1.Controls.Add(m_wb);
            this.panel2.Controls.Add(m_wb_2);
            this.panel3.Controls.Add(m_wb_3);
        }

        private void InitializeWb()
        {
            CefSettings settings = new CefSettings();
            settings.CefCommandLineArgs.Remove("process-per-tab");
            settings.CachePath = @"d:\cef\cache";
            Cef.Initialize(settings);

            m_wb = new ChromiumWebBrowser();
            m_wb_2 = new ChromiumWebBrowser();
            m_wb_3 = new ChromiumWebBrowser();

            var browser_settings = new BrowserSettings {
                Plugins = CefState.Disabled,
                WebGl = CefState.Disabled,
                RemoteFonts = CefState.Disabled,
                WindowlessFrameRate = 1,
                ImageLoading = CefState.Disabled,
            };

            m_wb.Dock = DockStyle.Fill;
            m_wb.BrowserSettings = browser_settings;

            
            m_wb_2.Dock = DockStyle.Fill;
            m_wb_2.BrowserSettings = browser_settings;

            
            m_wb_3.Dock = DockStyle.Fill;
            m_wb_3.BrowserSettings = browser_settings;
        }

        ChromiumWebBrowser m_wb, m_wb_2, m_wb_3;

        private void button1_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<string, string>> categorys;

            //ArticleIO.repairList("ubz_30.xlsx");
            //ArticleIO.mergeList(@"D:\Work\Project\Git\cs\CompanyParser\prod");
            //ArticleIO.fromPathToDb(@"D:\Work\Project\Git\cs\CompanyParser\prod\ru_all");

            //ParserRosfirm rosFirm = new ParserRosfirm();
            //categorys = CompanyParser.IO.ArticleIO.ReadCategoryUrl("category.xlsx", 3);
            //foreach (var entry in categorys)
            //{
            //    Console.WriteLine("{0} = {1}", entry.Value, entry.Key);
            //    rosFirm.parsingUrlOnPage(entry.Key, entry.Value);
            //}
            //List<Company> companys = ArticleIO.ReadXlsx(@"D:\Work\Project\Git\cs\CompanyParser\prod\ru_rosfirm\ru_rosfirm.xlsx");
            //int current_count = 0;
            //companys.ForEach(c => {
            //    rosFirm.repair_parse_phones(ref c);
            //});
            //ArticleIO.WriteXslx(companys, "repaired.xlsx");

            //ParserAllBiz allBiz_ru = new ParserAllBiz("https://ru.all.biz/enterprises");
            //categorys = CompanyParser.IO.ArticleIO.ReadCategoryUrl("category.xlsx", 2);
            //foreach (var entry in categorys)
            //{
            //    Console.WriteLine("{0} = {1}", entry.Value, entry.Key);
            //    allBiz_ru.parseAdFromFile(entry.Key, entry.Value);
            //}


            //ParserAllBiz allBiz_ua = new ParserAllBiz("https://ua.all.biz/enterprises", true);
            //categorys = CompanyParser.IO.ArticleIO.ReadCategoryUrl("category.xlsx", 6);
            //foreach (var entry in categorys)
            //{
            //    Console.WriteLine("{0} = {1}", entry.Value, entry.Key);
            //    allBiz_ua.parseAdFromFile(entry.Key, entry.Value);
            //}



            //PulcenFirms pulcen = new PulcenFirms("http://www.pulscen.ru/firms", ref m_wb, ref m_wb_2, ref m_wb_3);
            //pulcen.on_cleardata += () => {
            //    Cef.GetGlobalCookieManager().DeleteCookies();

            //    Console.WriteLine("Webbdrowser data removed.");
            //};
            //categorys = CompanyParser.IO.ArticleIO.ReadCategoryUrl("category.xlsx", 4);
            //categorys = new List<KeyValuePair<string, string>>();

            //File.ReadAllLines("pulscen_urls_list.txt").ToList().ForEach(url_raw =>
            //{

            //    string url = url_raw.Split(';')[0];
            //    string header = url_raw.Split(';')[1];

            //    categorys.Add(new KeyValuePair<string, string>(url, header));
            //});

            //foreach (var entry in categorys)
            //{
            //    Console.WriteLine(entry.Key);
            //    pulcen.ParsingURLS(entry.Key, entry.Value);
            //}





            //UkrBiz ukrBiz = new UkrBiz("http://ukr-biz.net/directory/");
            //categorys = CompanyParser.IO.ArticleIO.ReadCategoryUrl("category.xlsx", 7);
            //foreach (var entry in categorys)
            //{
            //    Console.WriteLine("{0} = {1}", entry.Value, entry.Key);
            //    ukrBiz.parsingAllCompany(entry.Key, entry.Value);
            //}


            Random rnd = new Random();
            var proxy_list = load_proxy("proxy_olx.txt");
            UaRegioInfo uaRegioInfo = new UaRegioInfo("https://www.ua-region.info/");
            categorys = CompanyParser.IO.ArticleIO.ReadCategoryUrl("category.xlsx", 2);
            foreach (var entry in categorys)
            {
                Again:

                Console.WriteLine("{0} = {1}", entry.Value, entry.Key);

                if (proxy_list.Count == 0)
                {
                    Console.WriteLine("empty proxy");
                    break;
                }

                var index = rnd.Next(proxy_list.Count);
                var proxy = proxy_list[index];

                if (!uaRegioInfo.parsingUrlsForCompany(entry.Key, entry.Value, proxy))
                {
                    proxy_list.RemoveAt(index);
                    goto Again;
                }

                Console.WriteLine("worked proxy: " + proxy);
            }



            //UbzCom ubz = new UbzCom();
            //categorys = CompanyParser.IO.ArticleIO.ReadCategoryUrl("category.xlsx", 8);
            //foreach (var entry in categorys)
            //{
            //    Console.WriteLine("{0} = {1}", entry.Value, entry.Key);
            //    ubz.LoadPages(entry.Key, entry.Value);
            //}

            //input_file_to_xlsx(@"D:\Downloads\МИШЕ.txt");

            Console.WriteLine("Парсинг завершен");
		}

        void input_file_to_xlsx(string @path)
        {
            var lines = File.ReadLines(path);

            var category_list = new Dictionary<string, List<string>>();

            string current_category = "";

            foreach (var line in lines)
            {
                if (line.StartsWith("укр"))
                {
                    current_category = line;
                    category_list.Add(line, new List<string>());

                } else if (line.StartsWith("http"))
                {
                    category_list[current_category].Add(line);
                }
            }

            var list = new List<string>();
            foreach (var category in category_list.Keys)
            {
                var urls = string.Join(", ", category_list[category]);
                var output = string.Format("{0};{1}", category, urls);
                //Console.WriteLine("{0};{1}", category, urls);
                list.Add(output);
            }

            File.WriteAllLines("../new_category.txt", list);

        }

        List<string> load_proxy(string @path)
        {
            var logFile = File.ReadAllLines(path);
            var logList = new List<string>(logFile);

            return logList;
        }
    }
}
