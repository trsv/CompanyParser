﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using OfficeOpenXml;

namespace CompanyParser.data
{
    class Settings
    {
        private string pathToSetting = @"Ключивые фразы и слова.xlsx";
        public List<string> keywords_trading = new List<string>(); // ключевые слова для торговых предприятий
        public List<string> keywords_of_production = new List<string>(); // ключевые слова производства
        public List<string> service_keywords = new List<string>(); // ключевые слова услуги
        public List<string> operator_of_telephones_mobileUKR = new List<string>(); // операторы телефонов Украины мобильных
        public List<string> operator_of_telephones_cityURK = new List<string>(); // операторы телефонов Украины городские
        public List<string> operator_of_telephones_mobile_RUS = new List<string>(); // операторы телефонов России мобильных
        public List<string> operator_of_telephones_city_RUS = new List<string>(); // операторы телефонов Украины городские
        public Dictionary<string,string> regions_russian = new Dictionary<string, string>(); // области и города России
        public Dictionary<string,string> regions_ukr = new Dictionary<string, string>(); // области и города Украины 
        public Dictionary<string, string> code_country = new Dictionary<string, string>(); // список кодов стран мира
        public HashSet<string> emails = new HashSet<string>();

        public Settings()
        {
            LoadSettings();
            LoadCodes();
            LoadEmails();

            operator_of_telephones_mobileUKR = operator_of_telephones_mobileUKR.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList();
            operator_of_telephones_mobile_RUS = operator_of_telephones_mobile_RUS.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList();
		}

        private void LoadCodes() {
			using (var reader = new StreamReader(@"codes.txt"))
			{
				while (!reader.EndOfStream)
				{
					var line = reader.ReadLine();
					var values = line.Split(';');

                    code_country.Add(values[0], values[1]);
				}
			}
        } 

        private void LoadEmails()
        {
            using (var reader = new StreamReader(@"emails.txt"))
            {
                while (!reader.EndOfStream)
                {
                    string email = reader.ReadLine();
                    email = email.ToLower().Trim();

                    emails.Add(email);
                }
            }
        }

        private void LoadSettings()
        {

            try
            {
                List<string> list = new List<string>();

                using (FileStream fileStream = new FileStream(pathToSetting, FileMode.Open))
                {
                    ExcelPackage excel = new ExcelPackage(fileStream);
                    var sheet = excel.Workbook.Worksheets.First();
                    var startRow = 2;

                    for (int row = startRow; row <= sheet.Dimension.End.Row; row++)
                    {
                        try
                        {
                            string value = "", value2 = "";

                            value = GetValue(row, 1, ref sheet);
                            if(!String.IsNullOrEmpty(value))
                                keywords_trading.Add(value);

                            value = GetValue(row, 2, ref sheet);
                            if (!String.IsNullOrEmpty(value))
                                keywords_of_production.Add(GetValue(row, 2, ref sheet));

                            value = GetValue(row, 3, ref sheet);
                            if (!String.IsNullOrEmpty(value) )
                                service_keywords.Add(GetValue(row, 3, ref sheet));
                            value = GetValue(row, 5, ref sheet);
                            if (!String.IsNullOrEmpty(value))
                                operator_of_telephones_mobile_RUS.Add(GetValue(row, 5, ref sheet));

                            string [] ukr = GetValue(row, 7, ref sheet).Split(' ');
                            value = GetValue(row, 7, ref sheet);
                            if (!String.IsNullOrEmpty(value) && value.IndexOf(' ') == -1)
                                operator_of_telephones_mobileUKR.Add(value);
                            if (ukr.Length != 0)
                                operator_of_telephones_mobileUKR.AddRange(ukr);

                            value = GetValue(row, 10, ref sheet);
                            value2 = GetValue(row, 11, ref sheet);
                            if (!String.IsNullOrEmpty(value) || !String.IsNullOrEmpty(value2))
                                regions_russian.Add(value, value2);
                            value = GetValue(row, 12, ref sheet);
                            value2 = GetValue(row, 13, ref sheet);
                            if (!String.IsNullOrEmpty(value) || !String.IsNullOrEmpty(value2))
                                regions_ukr.Add(value, value2); 
                        }
                        catch (NullReferenceException)
                        {
                            break;
                        }

                        //numbers.Where(n => !string.IsNullOrEmpty(n)).ToArray();
                    }

                    Console.Write("HelloWorld!!");
                }
                
            }
            catch (FileNotFoundException e)
            {
                throw new FileNotFoundException("Файл с артикулами не найден", e);
            }
        }

        private string GetValue(int row, int column, ref ExcelWorksheet sheet)
        {
            string result;
            try
            {
                result = sheet.Cells[row, column].Value.ToString().Trim();
                return result;
            }
            catch(Exception)
            {
                return "";
            }
        }
    }
}
