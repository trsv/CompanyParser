﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CompanyParser.data
{
    class Company
    {
        public string name_company { get; set; } // название компании
        //public int id { get; set; } // № порядковый
        public string source { get; set; } // источник
        public string section_in_source { get; set; } // раздел в источнике
        public string link_to_car_in_directory { get; set; } // Ссылка на карточку в каталоге
        public string ad_date { get; set; } // дата объявления
        public string category { get; set; } // категория
        public string group { get; set; } // группа 
        public string view { get; set; } // вид
        public string legal_name { get; set; } // юридическое название
        public string phone_mob { get; set; } // мобильный телефон
        public string phone_city { get; set; } // городской телефон
        public string email { get; set; }
        public string Facebook_url { get; set; }
        public string url_company { get; set; } // сайт компании
        public string type_company { get; set; } // Тип компании (Производство или торговое предприятие или услуги)
        public string number_product_on_site { get; set; } // количество товаров на сайте
        public string Availability_list_products_with_prices_on_site { get; set; } //Наличие списка товаров с ценами на сайте
        public string region { get; set; } // регион
        public string District_city { get; set; } // район (город)
        public string For_large_cities_district_of_city { get; set; } // Для крупных городов район города
        public string address { get; set; } // адресс
        public string Rating_of_trust_points { get; set; } // Рейтинг доверия (баллы)
        public string description { get; set; } // описание компании
        public string short_description_about_company { get; set; } // краткое описание о компании
        public string Additional_Information { get; set; } // дополнительная информация о компании
        public string Activities { get; set; } // виды деятельности
        public string company_news { get; set; } // Новости компании
        public string Note { get; set; } // примечания
        public string Country { get; set; } // страна
        public string Keyword { get; set; } // ключевые слова
        public string Id_Pulce { get; set; } // id для донора Pulse
        public string url_category { get; set; }

        public void toUtf8() {
            foreach (PropertyInfo property in this.GetType().GetProperties())
			{
                if (property.PropertyType == typeof(string)) {
                    byte[] bytes = Encoding.Default.GetBytes((string)property.GetValue(this));
                    string utf = Encoding.UTF8.GetString(bytes);
                    property.SetValue(this, utf); 
                }
			}
        }

        public void print() {
            Console.WriteLine("--- new company ---");
            foreach (PropertyDescriptor desc in TypeDescriptor.GetProperties(this)) {
                Console.WriteLine("{0} = {1}", desc.Name, desc.GetValue(this));
            }
        }

        public override int GetHashCode()
        {
            int hash = 0;

            foreach (PropertyInfo property in this.GetType().GetProperties())
            {
                if (property.PropertyType == typeof(string))
                {
                    string value = (string)property.GetValue(this, null);
                    if (!string.IsNullOrWhiteSpace(value)) hash += value.GetHashCode();
                }
            }

            return hash;
        }

        public override bool Equals(object o)
        {
            return ((Company)o).GetHashCode() ==  this.GetHashCode();
        }
    }
}
