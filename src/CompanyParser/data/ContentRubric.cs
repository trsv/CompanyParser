﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyParser.data
{
    class ContentRubric
    {
        public string general_url = "http://www.rosfirm.ru/catalog";
        public List<RosfirmContent> allContent = new List<RosfirmContent> ();

        public List<string> list_Urls_On_Page = new List<string>(); // список ссылок на компании с одной страницы 
        public List<Company> list_company = new List<Company>();
    }
}
