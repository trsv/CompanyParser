﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyParser.data
{
    class RosfirmContent
    {
        public string section_title; // название раздела
        public string section_url; // ссылка на раздел
        public Dictionary<string, string> subsections_and_their_references = new Dictionary<string, string>(); // название под разделов и ссылки на них
        public List<RosfirmContent> children_list_content = new List<RosfirmContent>();
    }
}
