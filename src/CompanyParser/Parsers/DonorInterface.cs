﻿using CompanyParser.data;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Threading.Tasks;

namespace CompanyParser.Parsers
{
    abstract class DonorInterface
    {
        public const string USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36";

        public HtmlWeb m_web { get; set; }
        public HtmlDocument m_doc { get; set; }
        public HashSet<string> m_options { get; set; } = new HashSet<string>();

        public DonorInterface(bool encoding_windows = false)
        {
            if (encoding_windows)
            {
                m_web = new HtmlWeb
                {
                    AutoDetectEncoding = false,
                    OverrideEncoding = Encoding.GetEncoding("windows-1251")
                };
            }
            else
            {
                m_web = new HtmlWeb();
            }
        }
             
        public void print_options()
        {
            foreach (string option in m_options) Console.WriteLine("Опция: {0}", option);
            Console.WriteLine("Количество: {0}", m_options.Count);
        }

        public string getSingleValue(string xpath)
		{
			string single_value;

			try
			{
				single_value = m_doc.DocumentNode.SelectSingleNode(xpath).InnerText.Trim();
			}
			catch (Exception)
			{
				single_value = string.Empty;
			}

			return HttpUtility.HtmlDecode(single_value);
		}

        public void parseEmailAndFacebook(ref Company comp)
        {
            if (!string.IsNullOrWhiteSpace(comp.url_company))
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(comp.email))
                    {
                        Console.WriteLine("Ищем почту на: {0}", comp.url_company);

                        m_doc = m_web.Load(comp.url_company);
                        comp.email = m_doc.DocumentNode.InnerHtml.extractFullMatch();
                    }

                    if (string.IsNullOrWhiteSpace(comp.Facebook_url))
                    {
                        Console.WriteLine("Ищем facebook на: {0}", comp.url_company);

                        comp.Facebook_url =
                            Regex.Match(m_doc.DocumentNode.InnerHtml, @"(https?:\/\/)?((w{3}\.)?)facebook\.com\/(?:[^\s()\\\[\]{};:'"""",<>?«»“”‘’]){5,}").Value;

                        if (comp.Facebook_url.Equals("https://www.facebook.com/pages/") || comp.Facebook_url.Equals("http://www.facebook.com/share.php"))
                            comp.Facebook_url = string.Empty;
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Не смогли зайти на сайт");
                }
            }
        }
    }

    /// <summary>
    /// Набор дополнительных методов для обработки данных
    /// </summary>
    internal static class Utils
    {

        private const string PATH_IMAGE = @"image_new/";
        private const string EMAIL_VALIDATE = @"(?:[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*|""(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*"")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])";

        public static string extract(this string value, string regex, int group = 1)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(value)) return string.Empty;
                else return Regex.Match(value, regex).Groups[group].Value;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

		public static string extractFullMatch(this string value)
		{
			try
			{
                string regex = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";

				if (string.IsNullOrWhiteSpace(value)) return string.Empty;
                else {
                    int limit = 2, current = 0;
                    HashSet <string> emails = new HashSet<string>();

					foreach (Match m in Regex.Matches(value, regex))
					{
                        string email = m.Value;

                        if (validateEmail(email) && email.Length < 20)
                        {
                            if (!email.Contains("loading") && !email.Contains(".gif")) emails.Add(email);
                            current++;
                        }

                        if (current == limit) break;
					}

                    return string.Join(" ", emails).Trim();
                }
			}
			catch (Exception)
			{
				return string.Empty;
			}
		}

        private static bool validateEmail(string email) {
            return Regex.IsMatch(email, EMAIL_VALIDATE);
        }

        public static string clear(this string source, string @regex)
        {
            return Regex.Replace(source, regex, string.Empty).Trim();
        }

        public static int randomPause(int a, int b)
        {
            int pause;
            try
            {
                pause = new Random().Next(a, b);
            }
            catch (Exception)
            {
                pause = 30;
            }

            pause *= 1000;

            Console.WriteLine("[INFO] Thread pause: {0}", pause);

            return pause;
        }
    }
}
