﻿using CompanyParser.data;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using CompanyParser.IO;
using System.IO;
using System.Net.Http;

namespace CompanyParser.Parsers
{
    class ParserAllBiz : DonorInterface
    {
        string m_url;
        string m_source;
        public ContentRubric myContent = new ContentRubric();
        List<Company> m_company_list = new List<Company>();
        public List<string> m_error_list = new List<string>();
        string m_local;

        //private Dictionary

        public ParserAllBiz(string Url, bool ua = false)
        {
            m_url = Url;
            m_web.UseCookies = true;
            m_source = ua ? "ua.all.biz" : "ru.all.biz";
            m_local = ua ? "ua" : "ru";

            m_web.Load("https://ua.all.biz/");
        }

        public void repairList()
        {
            List<Company> list = ArticleIO.ReadXlsx("allbiz.xlsx");

            list.ForEach(c =>
            {
                c.do_filter(true);
            });

            ArticleIO.WriteXslx(list, "allbiz-new.xlsx");
        }

        public void parseCategoryUrl(string url)
        {

            m_doc = m_web.Load(url);

            string category = m_doc.DocumentNode.SelectSingleNode("//div[@class='ab-breadcrumbs']/span[last()]/samp").InnerText;

            List<string> url_category_list = new List<string>();
            List<string> url_category_list2 = new List<string>();

            try
            {
                foreach (HtmlNode a in m_doc.DocumentNode.SelectNodes("//div[@class='b-markets__category-list_item_top']/a"))
                {
                    string category_url = a.Attributes["href"].Value;

                    url_category_list2.Add(category_url);
                }
            }
            catch (NullReferenceException)
            {
                url_category_list2.Add(url);
            }

            url_category_list2.ForEach(url2 =>
            {
                m_doc = m_web.Load(url2);

                try
                {
                    foreach (HtmlNode a in m_doc.DocumentNode.SelectNodes("//div[@class='b-markets__category-list_item_top']/a"))
                    {
                        string category_url = a.Attributes["href"].Value;

                        url_category_list.Add(category_url);
                    }
                }
                catch (NullReferenceException)
                {
                    url_category_list.Add(url);
                }
            });

            List<string> pages = new List<string>();

            try
            {

                foreach (string category_url in url_category_list)
                {
                    m_doc = m_web.Load(category_url);

                    foreach (HtmlNode a in m_doc.DocumentNode.SelectNodes("//a[@class='ab-markets-block__link']"))
                    {
                        pages.Add(a.Attributes["href"].Value + ";" + category);
                    }
                }

            }
            catch (NullReferenceException)
            {
                Console.WriteLine("!!! {0}", url);
                m_error_list.Add(url);
            }

            var bytes = System.Text.Encoding.UTF8.GetBytes(url);
            string path = string.Format("allbiz/{0}/{1}.txt", m_local, Convert.ToBase64String(bytes));

            if (pages.Count > 0)
            {
                //File.WriteAllLines(path, pages);
            }

            Console.WriteLine(path);

            //pages.ForEach(page => Console.WriteLine("---> {0}", page));

            Console.WriteLine("Saved");

        }

        public void parseGeneralPage()
        {
            m_doc = m_web.Load(m_url);
            try
            {
                foreach (HtmlNode node in m_doc.DocumentNode.SelectNodes("//div[contains(@class,'markets') and contains(@class, 'top')]/a"))
                {
                    RosfirmContent content = new RosfirmContent();
                    content.section_title = node.InnerText;
                    content.section_url = node.Attributes["href"].Value;
                    myContent.allContent.Add(content);
                    break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка при парсинге главной страницы: \n{0}", e);
            }
        }

        public void parseGeneralPages()
        {
            for (int i = 0; i < myContent.allContent.Count; i++)
            {
                m_doc = m_web.Load(myContent.allContent[i].section_url);
                foreach (HtmlNode node in m_doc.DocumentNode.SelectNodes("//div[contains(@class,'markets') and contains(@class, 'top')]/a"))
                {
                    RosfirmContent content = new RosfirmContent();
                    content.section_title = node.InnerText;
                    content.section_url = node.Attributes["href"].Value;
                    myContent.allContent[i].children_list_content.Add(content);
                }
            }
        }

        public void parsingPages()
        {
            for (int j = 0; j < myContent.allContent.Count; ++j)
            {
                for (int i = 0; i < myContent.allContent[j].children_list_content.Count; ++i)
                {

                    string next_page = myContent.allContent[j].children_list_content[i].section_url;

                    string dir = myContent.allContent[j].section_title + myContent.allContent[j].children_list_content[i].section_title;

                    do
                    {
                        Console.WriteLine("Парсим страницу: {0}", next_page);
                        m_doc = m_web.Load(next_page);
                        try
                        {
                            next_page = m_doc.DocumentNode.SelectSingleNode("//a[@class='b-paging__item b-paging__item_next']").Attributes["href"].Value;
                        }
                        catch (NullReferenceException)
                        {
                            next_page = string.Empty;
                        }

                        //ParsingAllUrl(dir);

                    } while (!string.IsNullOrWhiteSpace(next_page));
                }
            }
        }

        private bool ParsingAllUrl(string url, string url_category, string category, string section)
        {

            try
            {
                Company company = new Company();

                company.url_category = url_category;
                company.category = category;
                company.section_in_source = section;

                company.source = m_source;
                company.link_to_car_in_directory = url;

                m_doc = m_web.Load(url);

                Console.WriteLine("Парсим компанию: {0}", url);

                m_doc = m_web.Load(url);
                parseCompanyPageGeneral(ref company);

                m_doc = m_web.Load(url + "/info-about");
                parseCompanyPageAbout(ref company);

                m_doc = m_web.Load(url + "/contacts");
                parseCompanyPageContact(ref company);

                m_doc = m_web.Load(url + "/goods");
                parseCompanyPageGoods(ref company);

                company.do_filter(true);

                m_company_list.Add(company);

                return true;

            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] Ошибка при парсинге: {0}\n{1}", url, e);
                return true;
            }
        }


        public void parseFiles(string url_category)
        {
            try
            {
                var bytes = System.Text.Encoding.UTF8.GetBytes(url_category);
                string url_category_base64 = Convert.ToBase64String(bytes);
                string path = string.Format("allbiz/{0}/{1}.txt", m_local, url_category_base64);

                List<string> url_cat_raw = File.ReadAllLines(path).ToList();

                Dictionary<string, string> url_cat = new Dictionary<string, string>();

                url_cat_raw.ForEach(raw =>
                {
                    string[] line = raw.Split(';');
                    url_cat.Add(line[0], line[1]);
                });

                HashSet<string> url_company = new HashSet<string>();

                foreach (var entry in url_cat)
                {
                    Console.WriteLine("{0} = {1}", entry.Key, entry.Value);

                    try
                    {

                        string next_page = entry.Key;

                        while (!string.IsNullOrWhiteSpace(next_page))
                        {
                            Console.WriteLine("Next page: {0}", next_page);
                            m_doc = m_web.Load(next_page);

                            try
                            {
                                next_page = m_doc.DocumentNode.SelectSingleNode("//a[contains(@class, 'b-paging__item') and contains(@class, 'b-paging__item_next')]").Attributes["href"].Value;
                            }
                            catch (NullReferenceException)
                            {
                                next_page = string.Empty;
                            }

                            try
                            {

                                foreach (HtmlNode product in m_doc.DocumentNode.SelectNodes("//a[@class='company-name__link']"))
                                {
                                    string company_url = product.Attributes["href"].Value;

                                    url_company.Add(string.Format("{0};{1}", company_url, entry.Value));

                                    //Console.WriteLine("Company: {0}", company_url);
                                }

                            }
                            catch (Exception) { }

                        }

                    }
                    catch (Exception e)
                    {
                        File.AppendAllText("allbiz/ua/log.txt", string.Format("{0}\n", e));
                    }
                }

                File.WriteAllLines(string.Format("allbiz/{0}/company/{1}.txt", m_local, url_category_base64), url_company);

            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("!!! {0}", url_category);
            }
        }


        private void parseCompanyPageGeneral(ref Company company)
        {
            //company.description = getSingleValue("//div[@class='ms-content-block__data_about-main']");
            //company.short_description_about_company = getSingleValue("//div[@id='allbiz_seotext_tag']");

            try
            {
                company.email = m_doc.DocumentNode.SelectSingleNode("//meta[@itemprop='email']").Attributes["content"].Value;
            }
            catch (NullReferenceException) { }

            company.legal_name = m_doc.DocumentNode.SelectSingleNode("//meta[@itemprop='name']").Attributes["content"].Value;
        }

        private void parseCompanyPageAbout(ref Company company)
        {
            company.description = getSingleValue("//div[@class='ms-info-page__content']");

            if (string.IsNullOrWhiteSpace(company.description)) company.description = getSingleValue("//div[@class='b-info-text']");
            //company.short_description_about_company = getSingleValue("//table[@class='ms-info-page__additional']");

            company.Additional_Information = getSingleValue("//table[@class='ms-info-page__additional']");

            bool isMan = company.Additional_Information.ToLower().Contains("типпроизводство");

            if (isMan) company.type_company = "Производство";
        }

        private void parseCompanyPageContact(ref Company company)
        {
            company.Country = getSingleValue("//span[@class='country-name']");
            company.region = getSingleValue("//span[@class='region']");
            company.address = company.region + ", " + getSingleValue("//span[@class='locality']") + ", " + getSingleValue("//span[@class='street-address']");

            try
            {
                company.phone_city = parsePhone(company.link_to_car_in_directory);
                Console.WriteLine("phones: {0}", company.phone_city);
            } catch (NullReferenceException e) { Console.WriteLine("No phones: {0}", e); }
        }

        private void parseCompanyPageGoods(ref Company company)
        {
            bool hasPrices = !string.IsNullOrWhiteSpace(getSingleValue("//span[@class='ms-price__native']"));

            if (!hasPrices) hasPrices = !string.IsNullOrWhiteSpace(getSingleValue("//div[@class='b-product--list__price']"));

            company.Availability_list_products_with_prices_on_site = hasPrices ? "Да" : "Нет";

            try
            {
                int count = 0;
                foreach (HtmlNode p in m_doc.DocumentNode.SelectNodes("//span[@class='b-menu-top-sub-item__quantity']"))
                {
                    count += int.Parse(p.InnerText);
                }

                company.number_product_on_site = count.ToString();
            }
            catch (Exception) { }
        }

        public void parseAdFromFile(string url_category, string name_category)
        {
            m_company_list.Clear();

            var bytes = System.Text.Encoding.UTF8.GetBytes(url_category);
            string url_category_base64 = Convert.ToBase64String(bytes);
            string path = string.Format("allbiz/{0}/company/{1}.txt", m_local, url_category_base64);

            if (!File.Exists(path)) return;

            List<string> url_company_raw = File.ReadAllLines(path).ToList();

            Dictionary<string, string> url_company = new Dictionary<string, string>();

            url_company_raw.ForEach(raw =>
            {
                string[] line = raw.Split(';');
                url_company.Add(line[0], line[1]);
            });

            foreach (var entry in url_company)
            {
                ParsingAllUrl(entry.Key, url_category, name_category, entry.Value);
            }

            ArticleIO.WriteXslx(m_company_list, "text.xlsx");
        }

        private string parsePhone(string referer)
        {
            HashSet<string> phone_list = new HashSet<string>();

            foreach (HtmlNode item in m_doc.DocumentNode.SelectNodes("//div[@data-entid]"))
            {
                string ent_id = item.Attributes["data-entid"].Value;
                string phone = System.Web.HttpUtility.UrlEncode(item.Attributes["data-phone"].Value);
                string country = item.Attributes["data-country"].Value;

                string url = string.Format("https://api.all.biz/ajax/viewphone/ua?ent_id={0}&phone={1}&country={2}&source=minisite", ent_id, phone, country);
                string raw = getPhoneFromRequest(url, referer);

                List<string> formatted = raw.Split(',').ToList();

                formatted.ForEach(ph =>
                {
                    ph = ph.clear(@"[^\d+]");
                    phone_list.Add(ph);
                });
            }

            return string.Join(" ", phone_list);
        }

        private string getPhoneFromRequest(string url, string referer)
        {
            string phones = string.Empty;

            using (HttpClient client = new HttpClient())
            {
                Uri url_to = new Uri(url);

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("Host", url_to.Host);
                client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");
                client.DefaultRequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
                client.DefaultRequestHeaders.Add("Origin", referer);
                client.DefaultRequestHeaders.Add("Referer", referer + "/contacts");

                using (var response = client.GetAsync(url_to).Result)
                {
                    using (var content = response.Content)
                    {
                        string json = content.ReadAsStringAsync().Result;
                        phones = json.extract("phone\":\"(.*?)\"");
                    }
                }

            }

            return phones;
        }
    }
}
