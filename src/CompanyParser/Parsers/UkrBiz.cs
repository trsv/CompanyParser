﻿using CompanyParser.data;
using CompanyParser.IO;
using HtmlAgilityPack;
using System;
using System.Net;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;

namespace CompanyParser.Parsers
{
    class UkrBiz : DonorInterface
    {
        public ContentRubric myContent = new ContentRubric();
        string url;
        List<Company> m_list = new List<Company>();

        private HtmlDocument m_doc_helper = new HtmlDocument();

        public UkrBiz(string Url)
        {
            url = Url;
            m_doc = new HtmlDocument();
            m_web.AutoDetectEncoding = false;
            m_web.OverrideEncoding = System.Text.Encoding.GetEncoding("windows-1251");
        }
        // парсинг главное станицы сайта получение категорий
        public void parseGeneralPage()
        {
            m_doc = m_web.Load(url); ;
            try
            {
                Console.WriteLine("[UrkBiz] Парсим главную страницу");

                foreach (HtmlNode node in m_doc.DocumentNode.SelectNodes("//div[@id='contentBody']//tr//td/a"))
                {
                    RosfirmContent content = new RosfirmContent();
                    content.section_title = node.InnerText;
                    content.section_url = node.Attributes["href"].Value;

                    if (content.section_url.Contains("reg.php") || String.IsNullOrEmpty(content.section_title))
                        continue;
                    myContent.allContent.Add(content);
                    //break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка при парсинге главной страницы: \n{0}", e);
            }
        }
        // парсинг категорий сайта получение под категорий
        public void parsePagesUrl()
        {
            Console.WriteLine("[UrkBiz] Парсим подкатегории");

            for (int i = 0; i < myContent.allContent.Count; i++)
            {
                string urll = "http://ukr-biz.net/directory/" + myContent.allContent[i].section_url;

                m_doc = m_web.Load(urll);
                foreach (HtmlNode node in m_doc.DocumentNode.SelectNodes("//table[@class='submenu']//tr//td[@nowrap='']/a"))
                {
                    RosfirmContent content = new RosfirmContent();
                    content.section_title = node.InnerText;
                    content.section_url = node.Attributes["href"].Value;
                    myContent.allContent[i].children_list_content.Add(content);
                }
                break;
            }
        }
        // парсинг подкатегорий и компаний
        public void parsingAllCompany(string url_category, string category)
        {
            m_list.Clear();

            Console.WriteLine("[UrkBiz] Парсим весь контент");

            LoadPage(url_category, string.Empty);

            string car = m_doc.DocumentNode.SelectSingleNode("//h1").InnerText;

            Console.WriteLine("[UkrBiz] Парсим категорию: {0}", url_category);

            while (true)
            {
                if (!parsingTenCompany(car, url_category, category)) break;


                string urll = string.Empty;
                try
                {
                    urll = m_doc.DocumentNode.SelectSingleNode("//a[contains(., 'следующая')]").Attributes["href"].Value;
                }
                catch (NullReferenceException) { }

                if (String.IsNullOrEmpty(urll))
                    break;
                LoadPage(urll, "http://ukr-biz.net/directory/");
            }

            ArticleIO.WriteXslx(m_list, "ua_ukrbiz.xlsx");

        }
        // парсинг 10 предприятий 
        private bool parsingTenCompany(string car, string url_category, string category)
        {
            try
            {
                foreach (HtmlNode node in m_doc.DocumentNode.SelectNodes("//table[@class='mr15']/tr/td[2]"))
                {
                    Company company = new Company();

                    company.url_category = url_category;
                    company.category = category;
                    company.source = "ukr-biz.net";
                    company.section_in_source = car;


                    company.legal_name = getSingleValueFromNode(node, ".//h3");

                    try
                    {
                        string str1 = node.InnerHtml.Substring(node.InnerHtml.IndexOf("сайт:")).Replace("сайт:", string.Empty).Trim();
                        str1 = str1.Replace("<span>", string.Empty);
                        string value = str1.Substring(0, str1.IndexOf("<")).Trim();

                        if (Regex.IsMatch(value, @"&#x\d+;|&#x\d+[a-f];"))
                        {
                            value = Regex.Replace(value, @"[&#x;]+", string.Empty).Trim();
                            value = ConvertHexToString(value);
                        }

                        company.url_company = value.Trim().ToLower();
                    }
                    catch (Exception) { }

                    try
                    {
                        company.link_to_car_in_directory = node.SelectSingleNode(".//a[@title='Полный']").Attributes["href"].Value;
                    }
                    catch (Exception) { }

                    try
                    {
                        HashSet<string> phones = new HashSet<string>();

                        HtmlNode node_contact = node.SelectSingleNode(".//table");

                        try
                        {
                            company.region = getSingleValueFromNode(node_contact, ".//a[@class='region']");
                            company.address = getSingleValueFromNode(node_contact, ".//td[@width='100%']");
                        } catch { }

                        phones.Add(pre_formatPhone(getSingleValueFromNode(node_contact, ".//text()[contains(., 'тел./факс:')]")));
                        phones.Add(pre_formatPhone(getSingleValueFromNode(node_contact, ".//text()[contains(., 'моб.тел.:')]")));
                        phones.Add(pre_formatPhone(getSingleValueFromNode(node_contact, ".//text()[contains(., 'тел.:')]")));

                        var new_phones = phones.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList();
                        for (int i = 0; i < new_phones.Count; i++) if (!new_phones[i].StartsWith("+")) new_phones[i] = "+" + new_phones[i];

                        company.phone_city = string.Join(" ", new_phones);
                    }
                    catch (Exception) { }

                    try
                    {
                        string str1 = node.InnerHtml.Substring(node.InnerHtml.IndexOf("e-mail:")).Replace("e-mail:", string.Empty).Trim();
                        str1 = str1.Replace("<span>", string.Empty);
                        string value = str1.Substring(0, str1.IndexOf("<")).Trim();

                        if (Regex.IsMatch(value, @"&#x\d+;|&#x\d+[a-f];"))
                        {
                            value = Regex.Replace(value, @"[&#x;]+", string.Empty).Trim();
                            value = ConvertHexToString(value);
                        }

                        company.email = value.ToLower().Trim();
                    }
                    catch (Exception) { }

                    company.ad_date = getSingleValueFromNode(node, ".//text()[contains(., 'Дата:')]").Replace("Дата:", string.Empty).Trim();

                    try
                    {
                        string temp_url = "http://ukr-biz.net" + node.SelectSingleNode(".//a[1]").Attributes["href"].Value.Replace("..", "");
                        company.link_to_car_in_directory = temp_url.Contains("user") ? temp_url : string.Empty;
                    }
                    catch (Exception) { }

                    try
                    {
                        // если есть ссылка на отдельную компанию, можем зайти получить полное описание...
                        if (!String.IsNullOrEmpty(company.link_to_car_in_directory) && company.link_to_car_in_directory.Contains("user")) // если ссылка даже есть, она может быть на сайт компании ( нам переходить на нее не нужно)
                        {
                            LoadPage(company.link_to_car_in_directory, "http://ukr-biz.net", true);
                            company.description =
                                m_doc_helper.DocumentNode.SelectSingleNode("//div[@style='width:70%;']").InnerText
                                .Substring(
                                    0,
                                    m_doc_helper.DocumentNode.SelectSingleNode("//div[@style='width:70%;']").InnerText.IndexOf("Контакт:") - 2
                                    );
                        }
                        else
                        {
                            company.short_description_about_company = node.InnerText.Substring(0, node.InnerText.IndexOf("Контакт:")).Trim();
                        }
                    }
                    catch (Exception) { }

                    if (string.IsNullOrWhiteSpace(company.legal_name)) continue;

                    company.do_filter();

                    m_list.Add(company);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("[UrkBiz] Ошибка при парсинге компании");
            }

            return true;
        }

        private void LoadPage(string url, string url2, bool toHelper = false)
        {
            ;
            string urll = url2 + url; ;
            if (toHelper) m_doc_helper = m_web.Load(urll); /*m_doc_helper.LoadHtml(str);*/
            else m_doc = m_web.Load(urll); /*m_doc.LoadHtml(str);*/
        }

        private void LoadOnePage(string url)
        {
            string urll = "http://ukr-biz.net" + url; ;
            m_doc = m_web.Load(urll);
        }

        private static string ConvertHexToString(string HexValue)
        {
            string StrValue = "";
            while (HexValue.Length > 0)
            {
                StrValue += System.Convert.ToChar(System.Convert.ToUInt32(HexValue.Substring(0, 2), 16)).ToString();
                HexValue = HexValue.Substring(2, HexValue.Length - 2);
            }
            return StrValue;
        }

        private static string pre_formatPhone(string raw)
        {
            if (string.IsNullOrWhiteSpace(raw)) return string.Empty;

            string value = string.Empty;

            List<string> raws = raw.Split(',').ToList();

            for (int i = 0; i < raws.Count; i++) raws[i] = Regex.Replace(raws[i], @"\D", string.Empty);

            raws = raws.Where(s => !string.IsNullOrWhiteSpace(s) && s.Length < 15).Distinct().ToList();

            for (int i = 0; i < raws.Count; i++)
            {
                if (raws[i].StartsWith("0")) raws[i] = "+38" + raws[i];
                else if (raws[i].StartsWith("8") && !raws[i].StartsWith("8800")) raws[i] = "+7" + raws[i].Substring(1);

            }

            value = string.Join(" ", raws).Trim();

            return value;
        }

        private string getSingleValueFromNode(HtmlNode node, string xpath)
        {
            string value;

            try
            {
                value = node.SelectSingleNode(xpath).InnerText;
            }
            catch (Exception)
            {
                value = string.Empty;
            }

            return HttpUtility.HtmlDecode(value).Trim();
        }
    }
}

