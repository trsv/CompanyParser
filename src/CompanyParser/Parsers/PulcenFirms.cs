﻿using CompanyParser.data;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using CompanyParser.IO;
using CefSharp;
using CefSharp.WinForms;
using System.Threading;
using System.IO;
using System.Linq;

namespace CompanyParser.Parsers
{
    class PulcenFirms : DonorInterface
    {
        ChromiumWebBrowser m_wb, m_wb_2, m_wb_3;
        private CookieContainer cookies = new CookieContainer();
        public ContentRubric myContent = new ContentRubric();
        string m_url;

        string m_html_1, m_html_2, m_html_3;

        public delegate void ClearData();

        public event ClearData on_cleardata;

        public PulcenFirms(string Url, ref ChromiumWebBrowser wb, ref ChromiumWebBrowser wb_2, ref ChromiumWebBrowser wb_3)
        {
            m_wb = wb;
            m_wb_2 = wb_2;
            m_wb_3 = wb_3;
            m_doc = new HtmlAgilityPack.HtmlDocument();

            m_url = Url;
        }

        public void parseGeneralPageAsync()
        {
            loadPage(m_url);
            try
            {
                foreach (HtmlNode node in m_doc.DocumentNode.SelectNodes("//h3[@class='rb-header']"))
                {
                    RosfirmContent content = new RosfirmContent();
                    content.section_title = node.InnerText;
                    myContent.allContent.Add(content);
                    //break;
                }
                for (int i = 0; i < myContent.allContent.Count;)
                {
                    foreach (HtmlNode node in m_doc.DocumentNode.SelectNodes("//ul[@class='rb-list']"))
                    {

                        foreach (HtmlNode node3 in node.SelectNodes(".//li"))
                        {
                            RosfirmContent content = new RosfirmContent();
                            content.section_title = node3.InnerText;
                            content.section_url = node3.SelectSingleNode(".//a").Attributes["href"].Value;
                            foreach (HtmlNode node2 in node3.SelectNodes(".//div[2]/a"))
                            {
                                RosfirmContent content2 = new RosfirmContent();
                                if (String.IsNullOrEmpty(content.section_title))
                                {
                                    content.section_title = node2.SelectSingleNode(".//div[1]").InnerText;
                                    content.section_url = node2.SelectSingleNode(".//div[1]").Attributes["href"].Value;
                                }
                                content2.section_url = node2.Attributes["href"].Value;
                                content2.section_title = node2.InnerText;

                                content.subsections_and_their_references.Add(content2.section_title, content2.section_url);
                            }
                            myContent.allContent[i].children_list_content.Add(content);
                        }
                        i++;
                    }
                }

                Console.WriteLine("[Pulcen] Парсинг категорий успешно завершен");

                parsePagesUrl();
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка при парсинге главной страницы: \n{0}", e);
            }
        }

        public void parsePagesUrl()
        {
            Console.WriteLine("[Pulcen] Парсим по ссылкам...");

            for (int i = 0; i < myContent.allContent.Count; i++)
            {
                //if (i < 8)
                //    continue;
                for (int j = 0; j < myContent.allContent[i].children_list_content.Count; j++)
                { 
                    //if (myContent.allContent[i].children_list_content.Count > 5)
                    //    j += 4;
                    foreach (var h in myContent.allContent[i].children_list_content[j].subsections_and_their_references)
                    {
                        
                        string car1 = myContent.allContent[i].section_title.extract(@"([а-яА-Я, ]+)");
                        string car2 = myContent.allContent[i].children_list_content[j].section_title.extract(@"([а-яА-Я, ]+)");
                        string car = string.Format("{0} - {1} - {2}", car1, car2, h.Key);

                        //Console.WriteLine("{0} = {1}", h.Value, car);

                        ParsingURLS(h.Value, car);
                    }
                }
            }
        }


        public void ParsingURLS(string url_category, string name_category)
        {
            string next_page = url_category;
            List<Company> list = new List<Company>();
            int current_page = 1;


            do
            {
                Console.WriteLine("[Pulcen] Парсим список компаний: {0}", next_page);
                try
                {
                    loadPage(next_page);
                    HtmlNode nod1 = m_doc.DocumentNode;

                    string section = nod1.SelectSingleNode("//span[@class='bsi-link-content' and last()]").InnerText;

                    next_page = string.Format("{0}?page={1}", url_category, ++current_page);

                    foreach (HtmlNode nod in nod1.SelectNodes(".//div[@class='bp-content']"))
                    {
                        string url_current = nod.SelectSingleNode(".//li[@class='bpcl-item']/a").Attributes["href"].Value;

                        if (url_current.Contains("goto_online_store?utm_campaign")) continue;

                        Company company = new Company();
                        company.section_in_source = section;
                        company.category = name_category;
                        company.url_company = url_current;
                        company.url_category = url_category;

                        company.legal_name = nod.SelectSingleNode(".//ul/li/a").InnerText;
                        company.link_to_car_in_directory = company.url_company;

                        company.source = "www.pulscen.ru";

                        list.Add(company);
                    }

                    Console.WriteLine("Количетво компаний: {0}", list.Count);
                }
                catch (Exception e)
                {
                    Console.WriteLine("[PulsCen] Ошибка при парсинге страницы: {0}\n{1}", next_page, e);
                    File.AppendAllText("error_log_puls.txt", next_page);
                    next_page = string.Empty;
                }
            } while (!string.IsNullOrWhiteSpace(next_page));

            int current_index = 0;
            List<Company> temp_list = new List<Company>();
            list.ForEach(company =>
            {
                Console.WriteLine("Парсим {0} из {1}", ++current_index, list.Count);
                //if (current_index < 1350) { return; }

                DoWork(company.url_company, company.url_company + "/about", company.url_company + "/contacts");

                Console.WriteLine("[Pulcen] Парсим главную страницу: {0}", company.url_company);
                //loadPage(company.url_company);
                m_html_1 = m_wb.GetSourceAsync().Result;
                m_doc.LoadHtml(m_html_1);
                parseCompanyPageGeneral(ref company);

                Console.WriteLine("[Pulcen] Парсим о компании");
                //loadPage(company.url_company + "/about");
                m_html_2 = m_wb_2.GetSourceAsync().Result;
                m_doc.LoadHtml(m_html_2);
                parseCompanyPageAbout(ref company);
               
                Console.WriteLine("[Pulcen] Парсим контакты");
                //loadPage(company.url_company + "/contacts");
                m_html_3 = m_wb_3.GetSourceAsync().Result;
                m_doc.LoadHtml(m_html_3);
                parseCompanyPageContact(ref company);

                company.do_filter();

                temp_list.Add(company);
                if (temp_list.Count == 100)
                {
                    ArticleIO.WriteXslx(temp_list, "ru_pulscen.xlsx");
                    temp_list.Clear();
                    on_cleardata();
                }
            });

            ArticleIO.WriteXslx(temp_list, "ru_pulscen.xlsx");
            temp_list.Clear();
            on_cleardata();
        }


        private void parseCompanyPageGeneral(ref Company company)
        {
            company.Activities = getSingleValue("//div[contains(@class, 'company-business-types')]/div");
            company.type_company = company.Activities.Contains("Производитель") ? "Производство" : "Торговое предприятие";

            company.Availability_list_products_with_prices_on_site =
                string.IsNullOrWhiteSpace(getSingleValue("//div[@class='price']")) ? "Нет" : "Да";

            company.number_product_on_site = getSingleValue("//span[@class='mcmi-count']");

            try
            {
                foreach (HtmlNode node_news in m_doc.DocumentNode.SelectNodes("//a[@class='cn-header']"))
                {
                    company.company_news = node_news.Attributes["href"].Value + " " + node_news.InnerText.Trim() + "\n";
                }
            }
            catch (NullReferenceException) { }

        }

        private void parseCompanyPageAbout(ref Company company)
        {
            company.short_description_about_company = getSingleValue("//div[@id='company_about_inline_part_announce']/p");
            company.description = getSingleValue("//div[@id='company_about_full_description']//div[@class='editor-content']");
            company.Additional_Information = getSingleValue("//div[@id='company_about_inline_part_info']/ul");
        }

        private void parseCompanyPageContact(ref Company company)
        {
            HashSet<string> emails = new HashSet<string>();
            try
            {
                foreach (HtmlNode email_node in m_doc.DocumentNode.SelectNodes("//div[contains(@class, 'mail')]"))
                {
                    emails.Add(email_node.InnerText.Trim().extractFullMatch());
                }
                company.email = string.Join(" ", emails).Trim();
            }
            catch (NullReferenceException) { }

            HashSet<string> phones = new HashSet<string>();
            try
            {
                foreach (HtmlNode phone_node in m_doc.DocumentNode.SelectNodes("//div[@id='company-settings-contacts']/ul/li"))
                {
                    phones.Add(phone_node.InnerText.Trim());
                }
                company.phone_city = string.Join(",", phones).Trim();
                company.phone_city = company.phone_city.formatPhone();
            }
            catch (NullReferenceException) { }

            try
            {
                if (String.IsNullOrWhiteSpace(company.phone_city))
                {
                    foreach (HtmlNode phone_node in m_doc.DocumentNode.SelectNodes("//div[contains(@class, 'ci-phones') and contains(@class, 'flb-item') and contains(@class, 'secondary')]/div[@class='cip-item']/span[1]"))
                    {
                        phones.Add(phone_node.InnerText.Trim());
                    }
                    company.phone_city = string.Join(",", phones).Trim();
                    company.phone_city = company.phone_city.formatPhone();
                }
            }
            catch (Exception) { }

            company.address = getSingleValue("//div[@class='cia-short']").clear(@"^[^А-ЯA-Z]+");
        }

        private Task LoadPageAsync(string url)
        {
            var tcs = new TaskCompletionSource<bool>();

            EventHandler<LoadingStateChangedEventArgs> handler = null;
            handler = (sender, args) =>
            {
                //Wait for while page to finish loading not just the first frame
                if (!args.IsLoading)
                {
                    m_wb.LoadingStateChanged -= handler;
                    tcs.TrySetResult(true);
                }
            };

            m_wb.LoadingStateChanged += handler;

            if (!string.IsNullOrEmpty(url))
            {
                m_wb.Load(url);
            }

            return tcs.Task;
        }

        private void loadPage(string url)
        {
            LoadPageAsync(url).Wait();
            Task.Delay(3000).Wait();
            string html = m_wb.GetSourceAsync().Result;
            m_doc.LoadHtml(html);
        }

        private void DoSomething(string url)
        {
            Console.WriteLine("Parse url: {0}", url);

            if (url.EndsWith("/about")) LoadPageAsync_2(url).Wait();
            else if (url.EndsWith("/contacts")) LoadPageAsync_3(url).Wait();
            else LoadPageAsync(url).Wait();

            Task.Delay(2000).Wait();

            Console.WriteLine(")");
        }






        private void DoWork(string url1, string url2, string url3)
        {
            string[] urls = { url1, url2, url3 };
            var tasks = urls.Select(o => Task.Run(() => DoSomething(o))).ToArray();
            Task.WhenAll(tasks).Wait();
        }

        private void loadPage_1(string url)
        {
            LoadPageAsync(url).Wait();

            Console.WriteLine("Parsed already url: {0}", url);
        }



        private Task LoadPageAsync_2(string url)
        {
            var tcs = new TaskCompletionSource<bool>();

            EventHandler<LoadingStateChangedEventArgs> handler = null;
            handler = (sender, args) =>
            {
                //Wait for while page to finish loading not just the first frame
                if (!args.IsLoading)
                {
                    m_wb_2.LoadingStateChanged -= handler;
                    tcs.TrySetResult(true);
                }
            };

            m_wb_2.LoadingStateChanged += handler;

            if (!string.IsNullOrEmpty(url))
            {
                m_wb_2.Load(url);
            }

            return tcs.Task;
        }

        private void loadPage_2(string url)
        {
            LoadPageAsync_2(url).Wait();

            Console.WriteLine("Parsed already url: {0}", url);
        }



        private Task LoadPageAsync_3(string url)
        {
            var tcs = new TaskCompletionSource<bool>();

            EventHandler<LoadingStateChangedEventArgs> handler = null;
            handler = (sender, args) =>
            {
                //Wait for while page to finish loading not just the first frame
                if (!args.IsLoading)
                {
                    m_wb_3.LoadingStateChanged -= handler;
                    tcs.TrySetResult(true);
                }
            };

            m_wb_3.LoadingStateChanged += handler;

            if (!string.IsNullOrEmpty(url))
            {
                m_wb_3.Load(url);
            }

            return tcs.Task;
        }

        private void loadPage_3(string url)
        {
            LoadPageAsync_3(url).Wait();

            Console.WriteLine("Parsed already url: {0}", url);
        }
    }
}
