﻿using CompanyParser.data;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CompanyParser.IO;

namespace CompanyParser.Parsers
{
    class UaRegioInfo : DonorInterface
    {
        public ContentRubric myContent = new ContentRubric();
        string url;
        const string keyRekl = "Рекламная информация";
        const string keyProd = "Продукция, услуги";

        public UaRegioInfo(string Url) : base(false)
        {
            url = Url;
        }

        public void parseGeneralPage()
        {
            //WebClient wc = new WebClient();
            //string str = wc.DownloadString(url);
            m_doc = m_web.Load(url);
            try
            {
                foreach (HtmlNode node in m_doc.DocumentNode.SelectNodes("//div[@class='h-col-wrapper']"))
                {
                    foreach (HtmlNode node2 in node.SelectNodes(".//ul[contains(@class, 'b-catlist') and contains(@class, 'b-catlist-long')]"))
                    {
                        RosfirmContent content = new RosfirmContent();
                        try
                        {
                            content.section_title = node2.SelectSingleNode(".//li[contains(@class, 'item') and contains(@class, 'lvl-1')]/a[@class='item-link']").InnerText;
                            content.section_url = "https://www.ua-region.info" + node2.SelectSingleNode(".//li[contains(@class, 'item') and contains(@class, 'lvl-1')]/a[@class='item-link']").Attributes["href"].Value;

                            foreach (HtmlNode node3 in node2.SelectNodes(".//ul[@class='b-catlist-level-2']//li[@class='item']/a[@class='item-link']"))
                            {
                                RosfirmContent content2 = new RosfirmContent();
                                content2.section_title = node3.InnerText;
                                content2.section_url = "https://www.ua-region.info" + node3.Attributes["href"].Value;
                                content.children_list_content.Add(content2);
                            }
                        }
                        catch (Exception)
                        {
                            continue;
                        }
                        myContent.allContent.Add(content);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка при парсинге главной страницы: \n{0}", e);
            }
        }

        public bool parsingUrlsForCompany(string url_category, string category, string proxy)
        {
            List<Company> list = new List<Company>();

            int page_current = 1;

            //Company company = new Company();
            //m_doc = m_web.Load("https://www.ua-region.info/25412361");

            string proxy_socks = "socks5://" + proxy + "/";
            string proxy_http = "https://" + proxy + "/";

            try
            {
                m_doc = get_page(url_category, proxy, true);
            } catch (Exception e)
            {
                Console.WriteLine("error: {0}", e);
                return false;
            }

            //string section = m_doc.DocumentNode.SelectSingleNode("//h1[@class='b-company-rating-header']").InnerText;
            string nextPageStr = string.Empty;

            while (true)
            {

                nextPageStr = string.Format("{0}?start_page={1}", url_category, ++page_current);
                Console.WriteLine("========  След. страница: {0}", nextPageStr);

                try
                {
                    foreach (HtmlNode node in m_doc.DocumentNode.SelectNodes("//h2[@itemprop='name']"))
                    {
                        string url = "https://www.ua-region.info" + node.SelectSingleNode("./a").Attributes["href"].Value;
                        myContent.list_Urls_On_Page.Add(url);
                    }

                    Console.WriteLine("Ссылки на компании собраны со страницы");

                    // в цикле парситься компании 
                    for (int k = 0; k < myContent.list_Urls_On_Page.Count; k++)
                    {
                        //Console.WriteLine("[UaRegioInfo] Парсим {0} из {1}: {2}", k, myContent.list_Urls_On_Page.Count, myContent.list_Urls_On_Page[k]);

                        System.Threading.Thread.Sleep(1000);

                        //m_doc = m_web.Load(myContent.list_Urls_On_Page[k], "GET", proxy_o, credentials);

                        try
                        {
                            Console.WriteLine(myContent.list_Urls_On_Page[k]);
                            m_doc = get_page(myContent.list_Urls_On_Page[k], proxy);
                        } catch (Exception e)
                        {
                            Console.WriteLine("error: {0}", e);
                            return false;
                        }

                        HtmlNode node = m_doc.DocumentNode;
                        Company company = new Company();
                        company.url_category = url_category;
                        company.category = category;
                        company.legal_name = node.SelectSingleNode("//h1[@class='b-profile-header']").InnerText;

                        try
                        {
                            company.address = System.Web.HttpUtility.HtmlDecode(node.SelectSingleNode(
                                "//tr[./td[contains(@class, 'actual-address')]]/td[2]"
                                ).InnerText);
                            //Console.WriteLine("Юр. адрес: {0}", company.address);
                        }
                        catch (NullReferenceException) { company.address = string.Empty; }
                        if (string.IsNullOrWhiteSpace(company.address))
                        {
                            company.address = node.SelectSingleNode("//span[@itemprop='streetAddress']").InnerText;
                            //Console.WriteLine("Фак. адрес: {0}", company.address);
                        }


                        company.link_to_car_in_directory = myContent.list_Urls_On_Page[k];
                        int countTel = 0;
                        foreach (HtmlNode node3 in node.SelectNodes("//td[2 and @class='company-info-data']//table[@class='company-info-data-table']//tr"))
                        {

                            if (node3.InnerText.Contains("Телефон основной:") || node3.InnerText.Contains("Телефоны контактные:"))
                            {
                                countTel++;
                                if (countTel > 2)
                                    continue;
                                company.phone_city += node3.SelectSingleNode("./td[@class='data']").InnerText.Replace("&nbsp;", " ") + ",";
                            }
                            else if (node3.InnerText.Contains("Электронная почта:"))
                                company.email = getSingleNode(node3, "./td[@class='data']");
                            else if (node3.InnerText.Contains("Сайт:"))
                                company.url_company = getSingleNode(node3, "//td[@itemprop='url']");
                        }

                        foreach (HtmlNode node4 in node.SelectNodes("//div[@class='b-profile-block']"))
                        {
                            string str = node4.SelectSingleNode("./h3[contains(@class, 'b-profile-block-header') and contains(@class, 'collapse-title')]").InnerText;
                            if (node4.SelectSingleNode("./h3[contains(@class, 'b-profile-block-header') and contains(@class, 'collapse-title')]").InnerText.Contains("Деятельность предприятия"))
                            {
                                HtmlNode node6 = node4.SelectSingleNode("div[contains(@class,'b-profile-block-section') and contains(@class, 'collapse-body')]//ul");
                                foreach (HtmlNode node5 in node6.SelectNodes("./li/a"))
                                {
                                    company.Activities += node5.InnerText + "\n";
                                }
                                if (node4.InnerText.Contains(keyRekl))
                                {
                                    company.description = getValue(keyRekl, node4.InnerText);
                                }
                                if (node4.InnerText.Contains(keyProd))
                                {
                                    company.Additional_Information = getValue(keyProd, node4.InnerText);
                                }
                            }
                        }

                        company.phone_city = pre_formatPhone(company.phone_city);
                        company.Rating_of_trust_points = node.InnerHtml.extract(@"Внутренний рейтинг предприятия (\d+)");
                        company.ad_date = node.InnerText.extract(@"Дата проверки:(\d+\.\d+\.\d+)");
                        company.section_in_source = url_category;
                        company.source = "www.ua-region.info";
                        company.region = company.address.extract(@"(.*?),").clear(@"^[^А-Я]+");

                        //parseEmailAndFacebook(ref company);

                        company.do_filter();

                        //ArticleIO.writeToDb(company);
                        list.Add(company);

                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("[UaRegioInfo] Категория пуста: {0}", e);
                    nextPageStr = string.Empty;
                }

                myContent.list_Urls_On_Page.Clear();
                // получение следующей страницы

                if (string.IsNullOrWhiteSpace(nextPageStr)) break;
                m_doc = m_web.Load(nextPageStr);
            }

            ArticleIO.WriteXslx(list, "ua_regioninfo.xlsx");

            return true;

        }

        // парсинг xpath
        private string getSingleNode(HtmlNode node, string key)
        {
            string result = "";
            try
            {
                result = node.SelectSingleNode(key).InnerText;
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        // парсинг (просто вырезание по условию)
        private string getValue(string key, string str)
        {
            int start = str.IndexOf(key) + key.Length;
            int end = 0;

            string result = "";
            if (str.IndexOf(key) == -1)
                return result;
            if (key == "li class=\"item next\"><a class=\"item-link\" href=\"")
            {
                end = str.IndexOf("\"", start);
                result = str.Substring(start, end - start);
            }
            else if (key == keyRekl)
            {
                end = str.Length;
                result = str.Substring(start, end - start);
            }
            else if (key == keyProd)
            {
                end = str.Length;
                if (str.IndexOf(keyRekl) != -1)
                    end = str.IndexOf(keyRekl);
                result = str.Substring(start, end - start);
            }
            return result;
        }

        private static string pre_formatPhone(string raw)
        {
            if (string.IsNullOrWhiteSpace(raw)) return string.Empty;

            string value = string.Empty;

            List<string> raws = raw.Split(',').ToList();

            for (int i = 0; i < raws.Count; i++) raws[i] = Regex.Replace(raws[i], @"\D", string.Empty);

            raws = raws.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList();

            for (int i = 0; i < raws.Count; i++)
            {
                if (raws[i].StartsWith("0")) raws[i] = "+38" + raws[i];
                //else if (raws[i].StartsWith("8") && !raws[i].StartsWith("8800")) raws[i] = "+7" + raws[i].Substring(1);

            }

            value = string.Join(" ", raws).Trim();

            return value;
        }

        private HtmlAgilityPack.HtmlDocument get_page(string url, string proxy, bool from_file = false)
        {
            if (!from_file)
            {

                var wreq = WebRequest.Create(url);

                //'set the agent to mimic a recent browser
                //wreq.Hea = "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5";

                //'how you're getting the page
                wreq.Method = "GET";

                //'create a proxy
                wreq.Proxy = new WebProxy(proxy);

                //'set the proxy cred
                wreq.Proxy.Credentials = CredentialCache.DefaultCredentials;

                //'create the html doc & web
                var document = new HtmlAgilityPack.HtmlDocument();
                var web = new HtmlAgilityPack.HtmlWeb();

                //'needs to use cookies
                web.UseCookies = true;

                //'set the cookie request
                //wreq.Co = cookies;

                //'start a response
                var res = wreq.GetResponse();

                //'get a stream from the response
                document.Load(res.GetResponseStream(), true);

                return document;
            } else
            {
                var file_content = System.IO.File.ReadAllText(url);
                var document = new HtmlAgilityPack.HtmlDocument();
                document.LoadHtml(file_content);
                return document;
            }

        }

    }
}
