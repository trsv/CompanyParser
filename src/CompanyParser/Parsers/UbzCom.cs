﻿using CompanyParser.data;
using CompanyParser.Parsers;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CompanyParser.IO;

namespace CompanyParser.Parsers
{
    class UbzCom : DonorInterface
    {
        public ContentRubric myContent = new ContentRubric();

        public UbzCom()
        {
            m_web.UseCookies = true;
            m_web.PreRequest = onPreRequest;
        }

        private static bool onPreRequest(HttpWebRequest request)
        {
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            return true;
        }

        // получение главных страниц и категорий
        public void parseGeneralPageAsync()
        {
            m_doc = m_web.Load("http://www.ubz.com.ua/");

            Console.WriteLine("[UbzCom] Парсим главную страницу сайта");

            try
            {
                foreach (HtmlNode node in m_doc.DocumentNode.SelectNodes("//div[@class='category']"))
                {
                    RosfirmContent content = new RosfirmContent();
                    content.section_title = node.SelectSingleNode(".//h2[@class='title']//a[1]").InnerText;
                    content.section_url = "http://www.ubz.com.ua" + node.SelectSingleNode(".//h2[@class='title']//a[1]").Attributes["href"].Value;
                    foreach (HtmlNode node2 in node.SelectNodes(".//ul[@class='sub-categories']//li"))
                    {

                        RosfirmContent content2 = new RosfirmContent();
                        content2.section_title = node2.SelectSingleNode(".//a").InnerText;
                        content2.section_url = node2.SelectSingleNode(".//a").Attributes["href"].Value;

                        content.children_list_content.Add(content2);
                    }
                    myContent.allContent.Add(content);
                    //break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка при парсинге главной страницы: \n{0}", e);
                //Console.Write(m_doc.DocumentNode.InnerHtml);
                //MessageBox.Show(exception.Message, "Ошибка");
            }
        }


        public void parseSmallPageAsync()
        {
            Console.WriteLine("[UbzCom] Парсим подкатегории");

            try
            {
                for (int i = 0; i < myContent.allContent.Count; i++)
                {

                    for (int j = 0; j < myContent.allContent[i].children_list_content.Count; j++)
                    {
                        string url = "http://www.ubz.com.ua" + myContent.allContent[i].children_list_content[j].section_url;

                        Console.WriteLine("[UbzCom] Парсим: {0}", url);

                        m_doc = m_web.Load(url);
                        try
                        {
                            foreach (HtmlNode node in m_doc.DocumentNode.SelectNodes("//div[@class='category']"))
                            {
                                myContent.allContent[i].children_list_content[j].subsections_and_their_references.Add("http://www.ubz.com.ua" + node.SelectSingleNode(".//h2[@class='title']//a[1]").Attributes["href"].Value, node.SelectSingleNode(".//h2[@class='title']//a[1]").InnerText);
                            }
                        }
                        catch (Exception)
                        {

                        }

                        if (j == 5) break;
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка при парсинге второй страницы: \n{0}", e);
            }
        }

        public void LoadPages(string url_category, string category)
        {
            Console.WriteLine("[UbzCom] Парсим: {0}", url_category);

            List<Company> list = new List<Company>();

            string nextUrl = url_category;

            while (!string.IsNullOrWhiteSpace(nextUrl))
            {
                m_doc = m_web.Load(nextUrl);

                string section = m_doc.DocumentNode.SelectSingleNode("//h1[@class='title']").InnerText;

                try
                {
                    nextUrl = "http://www.ubz.com.ua" + m_doc.DocumentNode.SelectSingleNode("//a[@class='next']").Attributes["href"].Value;
                } catch (NullReferenceException) { nextUrl = string.Empty; }

                foreach (HtmlNode node in m_doc.DocumentNode.SelectNodes("//div[@class='teaser-item']"))
                {
                    try
                    {

                        Company company = new Company();
                        company.link_to_car_in_directory = "http://www.ubz.com.ua" + node.SelectSingleNode(".//h2[@class='pos-title']/a").Attributes["href"].Value;
                        company.legal_name = node.SelectSingleNode(".//h2[@class='pos-title']/a").Attributes["title"].Value;
                        company.source = "ubz.com.ua";
                        company.section_in_source = section;
                        company.category = category;
                        company.url_category = url_category;

                        try
                        {
                            company.phone_city = node.SelectSingleNode(".//p[./h21[contains(., 'Телефоны')]]").InnerText.Replace("Телефоны:", string.Empty).Trim();
                            company.phone_city = pre_formatPhone(company.phone_city);
                        }
                        catch (NullReferenceException) { }

                        m_doc = m_web.Load(company.link_to_car_in_directory);

                        company.Country = getSingleValue("//li[./strong[contains(., 'Страна')]]/text()[2]");
                        company.region = getSingleValue("//li[./strong[contains(., 'Область, район')]]/text()[2]");

                        company.address = getSingleValue("//li[./strong[contains(., 'Юридический адрес')]]/text()[2]");

                        if (string.IsNullOrWhiteSpace(company.address))
                        {
                            string city = getSingleValue("//li[./strong[contains(., 'Город')]]/text()[2]");
                            string street = getSingleValue("//li[./strong[contains(., 'Фактический адрес')]]/text()[2]");
                            company.address = string.Format("{0}, {1}", city, street).Trim();
                        }

                        company.address = company.address.clear(@"^[^А-ЯA-Z]+");

                        company.url_company = getSingleValue("//li[./strong[contains(., 'Официальный сайт')]]/a");
                        company.email = getSingleValue("//li[./strong[contains(., 'E-mail')]]/a");

                        try
                        {
                            foreach (HtmlNode a in m_doc.DocumentNode.SelectNodes("//div[./h3[contains(., 'Виды деятельности')]]//a"))
                            {
                                string value = a.InnerText.Trim();
                                company.Activities += value + "\n";
                            }

                            company.Activities = company.Activities.Trim();
                        }
                        catch (NullReferenceException) { }



                        company.do_filter();
                        list.Add(company);
                        
                    } catch
                    {
                        Console.WriteLine("Ошибка при парсинге компании");
                    }
                }

            }

            ArticleIO.WriteXslx(list, "ua_ubzcom.xlsx");

        }

        private static string pre_formatPhone(string raw)
        {
            if (string.IsNullOrWhiteSpace(raw)) return string.Empty;

            string value = string.Empty;

            List<string> raws = raw.Split(',').ToList();

            for (int i = 0; i < raws.Count; i++) raws[i] = Regex.Replace(raws[i], @"\D", string.Empty);

            raws = raws.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList();

            for (int i = 0; i < raws.Count; i++)
            {
                if (raws[i].StartsWith("0")) raws[i] = "+38" + raws[i];
                if (!raws[i].StartsWith("+")) raws[i] = "+" + raws[i];
                //else if (raws[i].StartsWith("8") && !raws[i].StartsWith("8800")) raws[i] = "+7" + raws[i].Substring(1);

            }

            value = string.Join(" ", raws).Trim();

            return value;
        }

    }
}
