﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using CompanyParser.data;
using HtmlAgilityPack;
using System.Web;
using CompanyParser.IO;

namespace CompanyParser.Parsers
{
    class ParserRosfirm : DonorInterface
    {
        public ContentRubric myContent = new ContentRubric();


        public void parseGeneralPage()
        {
            try
            {
                foreach (HtmlNode node in m_doc.DocumentNode.SelectNodes("//div[contains(@class, 'rubrik') and contains(@class, 'content')]"))
                {
                    RosfirmContent content = new RosfirmContent();
                    content.section_title = node.SelectSingleNode(".//span/a").InnerText;
                    content.section_url = node.SelectSingleNode(".//span/a").Attributes["href"].Value;
                    foreach (HtmlNode node2 in node.SelectNodes(".//ul/li/a"))
                    {
                        content.subsections_and_their_references.Add(node2.Attributes["href"].Value, node2.InnerText);
                    }
                    myContent.allContent.Add(content);
                    //break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка при парсинге главной страницы: \n{0}", e);
                //Console.Write(m_doc.DocumentNode.InnerHtml);
                //MessageBox.Show(exception.Message, "Ошибка");
            }
        }

        public void parseSmallPage()
        {
            try
            {
                for (int i = 0; i < myContent.allContent.Count; i++)
                {
                    string url = "http://www.rosfirm.ru" + myContent.allContent[i].section_url;
                    m_doc = m_web.Load(url);
                    Console.WriteLine("[INFO] Загрузили {0} из {1}: {2}", i + 1, myContent.allContent.Count, url);

                    try
                    {

                        foreach (HtmlNode node in m_doc.DocumentNode.SelectNodes("//div[contains(@class, 'rubrik') and contains(@class, 'content')]"))
                        {
                            RosfirmContent content = new RosfirmContent();
                            content.section_title = node.SelectSingleNode(".//span/a").InnerText;
                            content.section_url = node.SelectSingleNode(".//span/a").Attributes["href"].Value;

                            try
                            {
                                foreach (HtmlNode node2 in node.SelectNodes(".//ul/li/a"))
                                {
                                    content.subsections_and_their_references.Add(node2.Attributes["href"].Value, node2.InnerText);
                                }
                            }
                            catch (NullReferenceException)
                            {
                                content.subsections_and_their_references.Add(content.section_url, content.section_title);
                            }

                            myContent.allContent[i].children_list_content.Add(content);
                        }
                    }
                    catch (NullReferenceException)
                    {
                        RosfirmContent content = new RosfirmContent();

                        content.section_url = myContent.allContent[i].section_url;
                        content.section_title = myContent.allContent[i].section_title;

                        content.subsections_and_their_references.Add(content.section_url, content.section_title);

                        myContent.allContent[i].children_list_content.Add(content);
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка при парсинге второй страницы: \n{0}", e);
                //Console.Write(m_doc.DocumentNode.InnerHtml);
                //MessageBox.Show(exception.Message, "Ошибка");
            }
        }

        public void parsingUrlOnPage(string url_category, string category)
        {
            myContent.list_Urls_On_Page.Clear();

            try
            {

                string url = url_category;
                string section = string.Empty;

                while (true)
                {
                    try
                    {
                        Console.WriteLine("Парсим ссылки с: {0}", url);
                        m_doc = m_web.Load(url);

                        section = m_doc.DocumentNode.SelectSingleNode("//span[@class='rubrik-name']").InnerText;

                        Thread.Sleep(10);

                        foreach (HtmlNode node in m_doc.DocumentNode.SelectNodes("//a[@itemprop='url']"))
                        {
                            myContent.list_Urls_On_Page.Add(node.Attributes["href"].Value);

                            if (myContent.list_Urls_On_Page.Count == 30) break;
                        }


                        try
                        {
                            if (myContent.list_Urls_On_Page.Count == 30) url = string.Empty;
                            else url = m_doc.DocumentNode.SelectSingleNode("//a[contains(@class, 'next') and contains(@class, 'right') and contains(@class, 'arrow')]").Attributes["href"].Value;
                        }
                        catch (NullReferenceException)
                        {
                            url = string.Empty;
                        }

                        if (string.IsNullOrWhiteSpace(url)) break;
                        else url = "http://www.rosfirm.ru" + url;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Ошибка при парсинге компании: {0}\n{1}", url, e);
                        break;
                    }

                }

                for (int k = 0; k < myContent.list_Urls_On_Page.Count; k++)
                {
                    try
                    {
                        Console.WriteLine("Парсим компанию {0} из {1}: {2}", k + 1, myContent.list_Urls_On_Page.Count, myContent.list_Urls_On_Page[k]);

                        // Загрузка данных для компании
                        Company comp = new Company();

                        comp.category = category;
                        comp.url_category = url_category;

                        comp.link_to_car_in_directory = myContent.list_Urls_On_Page[k];
                        comp.source = "www.rosfirm.ru";
                        comp.section_in_source = section;
                        comp.ad_date = "";

                        m_doc = m_web.Load(myContent.list_Urls_On_Page[k]);
                        parseCompanyPageGeneral(ref comp);

                        m_doc = m_web.Load(myContent.list_Urls_On_Page[k] + "/info.htm");
                        parseCompanyPageInfo(ref comp);

                        m_doc = m_web.Load(myContent.list_Urls_On_Page[k] + "/contacts.htm");
                        parseCompanyPageContact(ref comp);

                        m_doc = m_web.Load(myContent.list_Urls_On_Page[k] + "/price.htm");
                        comp.Availability_list_products_with_prices_on_site =
                                string.IsNullOrWhiteSpace(getSingleValue("//div[@class='goods-price']")) ? "Нет" : "Да";

                        try
                        {
                            parseEmailAndFacebook(ref comp);
                        }
                        catch (Exception) { }

                        comp.do_filter();

                        ArticleIO.WriteXslx(comp, "text.xlsx");
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //MessageBox.Show(exception.Message, "Ошибка");
            }
        }

        private void parseCompanyPageGeneral(ref Company comp)
        {
            comp.Rating_of_trust_points =
                    getSingleValue("//div[@edit-data-link='certification']").extract(@"(\d+)");

            comp.short_description_about_company =
                getSingleValue("//div[@class='centerblock' and .//h2[contains(., 'Краткая информация о компании')]]/div");

            comp.Activities =
                getSingleValue("//div[@class='centerblock' and .//h2[contains(., 'Виды деятельности')]]/div");

            comp.description =
                    getSingleValue("//div[@class='about_left']");

            comp.number_product_on_site =
                    getSingleValue("//a[@class='black']/b[1]");

            comp.ad_date = getSingleValue("//div[@class='left_cont']/p/b[1]");

            try
            {
                foreach (HtmlNode news_node in m_doc.DocumentNode.SelectNodes("//div[@class='centerblock' and .//h2[contains(., 'Новости компании')]]/div/div/ul/li/a"))
                {
                    comp.company_news += news_node.Attributes["href"].Value;
                    comp.company_news += " " + news_node.InnerText;
                    comp.company_news += " ";
                }
            }
            catch (NullReferenceException) { }
        }

        private void parseCompanyPageInfo(ref Company comp)
        {
            comp.Additional_Information =
                    getSingleValue("//div[@class='centerblock' and .//h2[contains(., 'Дополнительная информация')]]/div");

            bool isMan = comp.Additional_Information.ToLower().clear(@"[^а-яА-Я]+").Contains("типкомпаниипроизводитель");

            if (isMan) comp.type_company = "Производство";
        }

        private void parseCompanyPageContact(ref Company comp)
        {
            comp.region =
                    getSingleValue("//div[@edit-data-link='change_info']//p[span[1 and contains(., 'Адрес')]]/span[2]//span[@class='region']");

            comp.District_city =
                    getSingleValue("//div[@edit-data-link='change_info']//p[span[1 and contains(., 'Адрес')]]/span[2]//span[@class='locality']");

            string street_company =
                getSingleValue("//div[@edit-data-link='change_info']//p[span[1 and contains(., 'Адрес')]]/span[2]//span[@class='street-address']");

            comp.address = comp.region + "," + comp.District_city + "," + street_company;

            comp.legal_name =
                    getSingleValue("//div[@edit-data-link='change_info']//p[span[1 and contains(., 'Юридическое название')]]/text()");

            comp.url_company =
                    getSingleValue("//div[@edit-data-link='change_info']//p[span[1 and contains(., 'Сайт')]]/a");


            try
            {
                foreach (HtmlNode node2 in m_doc.DocumentNode.SelectNodes("//div[@edit-data-link='change_info']//div/div//span[@class='tel']"))
                    comp.phone_city = node2.InnerText + ",";
                comp.phone_city = comp.phone_city.formatPhone();
            }
            catch (NullReferenceException) { }

            if (String.IsNullOrEmpty(comp.legal_name))
                comp.legal_name = getSingleValue("//div[@edit-data-link='change_info']//span[contains(@class, 'fn') and contains(@class, 'org')]");

        }

        public void repair_parse_phones(ref Company company)
        {
            company.phone_city = company.phone_city.formatPhone();
        }
    }
}
