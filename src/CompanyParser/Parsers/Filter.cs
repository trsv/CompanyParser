﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

using CompanyParser.data;

namespace CompanyParser.Parsers
{
    internal static class FilterWorker
    {

        private static Settings m_settings = new Settings();

        public static void do_filter(this Company comp, bool site_as_email = false)
        {
            comp.filter_type();
            comp.filter_telephone();
            comp.filter_region();
            comp.filter_country();
            comp.filter_address();

            if (site_as_email) comp.filter_site();
        }

        public static void do_post_filter(this Company comp)
        {
            comp.post_filter_phone();
        }

        private static void filter_site(this Company company)
        {
            company.url_company = string.Empty;

            if (string.IsNullOrWhiteSpace(company.email)) return;
            //if (!string.IsNullOrWhiteSpace(company.url_company)) return;

            string email = company.email.ToLower().Trim().extract(@"@(.*?)$").Split(' ')[0].Trim();

            if (!m_settings.emails.Contains(email))
            {
                company.url_company = email;
            }
        }

        private static void filter_type(this Company company)
        {
            if (string.IsNullOrWhiteSpace(company.type_company)) isProduction(ref company);
            if (string.IsNullOrWhiteSpace(company.type_company)) isTrading(ref company);
        }

        private static void isProduction(ref Company company) {
			List<string> raws;

            if (string.IsNullOrWhiteSpace(company.type_company) && !string.IsNullOrWhiteSpace(company.description))
			{
				raws = company.description.Split(' ').ToList();

				foreach (string word in raws)
				{
					if (m_settings.keywords_of_production.Contains(word))
					{
						company.type_company = "Производство";
                        //company.Keyword = word;
						break;
					}
				}
			}

			if (string.IsNullOrWhiteSpace(company.type_company) && !string.IsNullOrWhiteSpace(company.short_description_about_company))
			{
				raws = company.short_description_about_company.Split(' ').ToList();

				foreach (string word in raws)
				{
					if (m_settings.keywords_of_production.Contains(word))
					{
						company.type_company = "Производство";
						break;
					}
				}
			}

            if (string.IsNullOrWhiteSpace(company.type_company) && !string.IsNullOrWhiteSpace(company.Additional_Information))
			{
                raws = company.Additional_Information.Split(' ').ToList();

				foreach (string word in raws)
				{
					if (m_settings.keywords_of_production.Contains(word))
					{
						company.type_company = "Производство";
						break;
					}
				}
			}

            if (string.IsNullOrWhiteSpace(company.type_company) && !string.IsNullOrWhiteSpace(company.Activities))
            {
                raws = company.Activities.Split(' ').ToList();

                foreach (string word in raws)
                {
                    if (m_settings.keywords_of_production.Contains(word))
                    {
                        company.type_company = "Производство";
                        break;
                    }
                }
            }
        }

		private static void isTrading(ref Company company)
		{
			List<string> raws;

			if (string.IsNullOrWhiteSpace(company.type_company) && !string.IsNullOrWhiteSpace(company.description))
			{
				raws = company.description.Split(' ').ToList();

				foreach (string word in raws)
				{
                    if (m_settings.keywords_trading.Contains(word))
					{
						company.type_company = "Торговое предприятие";
						break;
					}
				}
			}

			if (string.IsNullOrWhiteSpace(company.type_company) && !string.IsNullOrWhiteSpace(company.short_description_about_company))
			{
				raws = company.short_description_about_company.Split(' ').ToList();

				foreach (string word in raws)
				{
                    if (m_settings.keywords_trading.Contains(word))
					{
						company.type_company = "Торговое предприятие";
						break;
					}
				}
			}

			if (string.IsNullOrWhiteSpace(company.type_company) && !string.IsNullOrWhiteSpace(company.Additional_Information))
			{
				raws = company.Additional_Information.Split(' ').ToList();

				foreach (string word in raws)
				{
					if (m_settings.keywords_trading.Contains(word))
					{
						company.type_company = "Торговое предприятие";
						break;
					}
				}
			}

            if (string.IsNullOrWhiteSpace(company.type_company) && !string.IsNullOrWhiteSpace(company.Activities))
            {
                raws = company.Activities.Split(' ').ToList();

                foreach (string word in raws)
                {
                    if (m_settings.keywords_trading.Contains(word))
                    {
                        company.type_company = "Торговое предприятие";
                        break;
                    }
                }
            }
        }

        public static string formatPhone(this string raw)
		{
			if (string.IsNullOrWhiteSpace(raw)) return string.Empty;

            Console.WriteLine("Format phone");

			List<string> raws = raw.Split(',').ToList();

			for (int i = 0; i < raws.Count; i++) raws[i] = Regex.Replace(raws[i], @"\D+", string.Empty);

            bool is_ua = raws[0].StartsWith("380");

            raws = raws.Where(s => !string.IsNullOrWhiteSpace(s) && s.Length > 6).Distinct().ToList();

			for (int i = 1; i < raws.Count; i++)
			{
                if (is_ua)
                {
                    raws[i] = string.Format("38{0}", Regex.Match(raws[i], @"(0\d+)"));
                } else {
                    string diff = Regex.Replace(raws[i - 1], @"\d{" + raws[i].Length + "}$", string.Empty);
                    if (!string.IsNullOrWhiteSpace(diff) && !raws[i].StartsWith(diff)) raws[i] = diff + raws[i];
                }
			}

            for (int i = 0; i < raws.Count; i++) if (!raws[i].StartsWith("8")) raws[i] = "+" + raws[i];


			return string.Join(" ", raws).Trim();
		}

        private static void filter_telephone(this Company company) {
            if (string.IsNullOrWhiteSpace(company.phone_city)) return;

            List <string> raws = company.phone_city.Split(' ').ToList();

            company.phone_city = string.Empty;

            raws.ForEach(phone => {

                if (string.IsNullOrWhiteSpace(company.Country))
                {
                    foreach (var entry in m_settings.code_country)
                    {
                        if (phone.StartsWith(entry.Key.Replace(" ", string.Empty))) {
                            company.Country = entry.Value;
                            break;
                        }
                    }
                }

                bool is_ru = phone.StartsWith("+7");
                bool is_ua = phone.StartsWith("+380");
                bool match = false;
                string phone_without_country;

                if (is_ua) {
                    phone_without_country = phone.Replace("+380", string.Empty);

					foreach (string ukr in m_settings.operator_of_telephones_mobileUKR)
					{
                        if (phone_without_country.StartsWith(ukr))
                        {
                            company.phone_mob += phone + " ";
                            match = true;
                            break;
                        }
					}
                } else if (is_ru){
                    phone_without_country = phone.Replace("+7", string.Empty);

					foreach (string rus in m_settings.operator_of_telephones_mobile_RUS)
					{
                        if (phone_without_country.StartsWith(rus))
                        {
                            company.phone_mob += phone + " ";
                            match = true;
                            break;
                        }
					}
                }

                if (!match) company.phone_city += phone + " ";
            });

            if (!string.IsNullOrWhiteSpace(company.phone_mob)) company.phone_mob = company.phone_mob.Trim();
            if (!string.IsNullOrWhiteSpace(company.phone_city))  company.phone_city = company.phone_city.Trim();

            Console.WriteLine("mobil: {0}", company.phone_mob);
            Console.WriteLine("citys: {0}", company.phone_city);
        }

        private static void filter_country(this Company compamy) {
            if (!string.IsNullOrWhiteSpace(compamy.Country)) return;

            if (!string.IsNullOrWhiteSpace(compamy.region))
            {

                string city_region = compamy.region.Split(' ')[0];

                if (m_settings.regions_russian.ContainsKey(city_region)) compamy.Country = "Россия";
                else if (m_settings.regions_ukr.ContainsKey(city_region)) compamy.Country = "Украина";
                else compamy.Country = string.Empty;
            } else if (!string.IsNullOrWhiteSpace(compamy.address))
            {
                string city_region = compamy.address.Split(',')[0];

                if (m_settings.regions_russian.ContainsKey(city_region)) compamy.Country = "Россия";
                else if (m_settings.regions_ukr.ContainsKey(city_region)) compamy.Country = "Украина";
                else compamy.Country = string.Empty;
            }
        }

        private static void filter_address(this Company company) {
            company.address = Regex.Replace(company.address, @"^[^а-яА-Яa-zA-Z0-9]+", string.Empty);
        }

        private static void filter_region(this Company company)
        {
            if (!string.IsNullOrWhiteSpace(company.region) || string.IsNullOrWhiteSpace(company.address)) return;

            if (Regex.IsMatch(company.address, @"[a-zA-Z]")) { company.region = "Европа, Китай"; return; }

            company.address = company.address.clear(@"^[^А-ЯA-Z]+");

            string city = company.address.Split(',')[0].Trim();

            if (city.StartsWith("Киев") || city.Equals("Київ")) { company.region = "Киевская область"; return; }

            if (m_settings.regions_ukr.ContainsKey(city)) company.region = m_settings.regions_ukr[city];
            else if (m_settings.regions_russian.ContainsKey(city)) company.region = m_settings.regions_russian[city];

            if (string.IsNullOrWhiteSpace(company.region) && city.Contains("обл")) company.region = city;
        }

        private static void post_filter_phone(this Company company)
        {
            List<string> raw_city = company.phone_city.Split(' ').ToList();
            List<string> raw_mob = company.phone_mob.Split(' ').ToList();

            raw_city = raw_city.Where(phone => phone.Length > 10).Distinct().ToList();
            raw_mob = raw_mob.Where(phone => phone.Length > 10).Distinct().ToList();

            raw_city.ForEach(phone => {
                if (phone.Length < 10) Console.WriteLine("Trash city: {0}", phone);
            });

            raw_mob.ForEach(phone => {
                if (phone.Length < 10) Console.WriteLine("Trash mobile: {0}", phone);
            });

            company.phone_city = string.Join(" ", raw_city).Trim();
            company.phone_mob = string.Join(" ", raw_mob).Trim();
        }
    }
}
