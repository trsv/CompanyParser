﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using OfficeOpenXml;
using CompanyParser.data;
using CompanyParser.Parsers;

namespace CompanyParser.IO
{
    class ArticleIO
    {
        private const string CONNECTION_STRING = "server=localhost;user=trsv;database=company_db;password=;charset=utf8mb4";
        private const string QUERY_INSERT = "INSERT INTO `company_table` (`country`,`source_site`,`source_dir`,`source_dir_url`,`source_date`,`category`,`group`,`vid`,`keyword`,`c_name`,`c_phone_mobile`,`c_phone_city`,`c_email`,`c_facebook`,`c_site`,`c_type`,`c_site_count`,`c_site_price`,`c_region`,`c_address`,`c_rating`,`c_about`,`c_about_short`,`c_about_add`,`c_act`,`c_news`,`c_note`) VALUES (@country,@source,@source_dir,@url_source_dir,@ad_date,@category,@group,@type,@keyword,@name_legal,@phone_mobile,@phone_city,@email,@url_facebook,@url_site,@type_company,@product_count,@has_price_list,@region,@address,@rating,@desc_full,@desc_short,@desc_add,@type_activitie,@news,@note);";

        private static MySqlConnection m_connection = new MySqlConnection(CONNECTION_STRING);
        private static MySqlCommand m_query;
        private static MySqlTransaction m_transaction;

        public static List<string> read(string @path, int column, bool hasHeader = false)
        {

            try
            {
                List<string> list = new List<string>();

                using (FileStream fileStream = new FileStream(path, FileMode.Open))
                {
                    ExcelPackage excel = new ExcelPackage(fileStream);
                    var sheet = excel.Workbook.Worksheets.First();
                    var startRow = hasHeader ? 2 : 1;

                    for (int row = startRow; row <= sheet.Dimension.End.Row; row++)
                    {
                        try
                        {
                            string value = sheet.Cells[row, column].Value.ToString().Trim();

                            list.Add(value);
                        }
                        catch (NullReferenceException)
                        {
                            break;
                        }

                    }
                }

                return list;
            }
            catch (FileNotFoundException e)
            {
                throw new FileNotFoundException("Файл с артикулами не найден", e);
            }
        }

        public static List<KeyValuePair<string, string>> ReadCategoryUrl(string @path, int url_column)
        {
            List <KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();

            using (FileStream fileStream = new FileStream(path, FileMode.Open))
            {
                ExcelPackage excel = new ExcelPackage(fileStream);
                var sheet = excel.Workbook.Worksheets.First();

                Console.WriteLine("[INFO] Читам файл с продуктами из {0}", path);

                try
                {
                    for (int row = 2; row <= sheet.Dimension.End.Row; row++)
                    {
                        string category = readValue(row, 1, ref sheet);
                        string url_raw = readValue(row, url_column, ref sheet);

                        if (url_raw.Equals("-")) continue;

                        List<string> url_list = url_raw.Split(',').ToList();

                        url_list.ForEach(url => {
                            url = url.Trim();
                            //url = System.Web.HttpUtility.UrlDecode(url);

                            if (string.IsNullOrWhiteSpace(url)) return;

                            list.Add(new KeyValuePair<string, string>(url, category));
                        });
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine("[ERROR] {0}", e);
                }
            }

            return list;
        }

        public static List<Company> ReadXlsx(string @path)
        {
            List<Company> companys = new List<Company>();

            using (FileStream fileStream = new FileStream(path, FileMode.Open))
            {
                ExcelPackage excel = new ExcelPackage(fileStream);
                var sheet = excel.Workbook.Worksheets.First();

                Console.WriteLine("[INFO] Читам файл с продуктами из {0}", path);

                try
                {
                    for (int row = 2; row <= sheet.Dimension.End.Row; row++)
                    {
                        Company c = new Company
                        {
                            Country = readValue(row, 1, ref sheet),

                            source = readValue(row, 3, ref sheet),
                            section_in_source = readValue(row, 4, ref sheet),
                            link_to_car_in_directory = readValue(row, 5, ref sheet),
                            ad_date = readValue(row, 6, ref sheet),
                            category = readValue(row, 7, ref sheet),
                            group = readValue(row, 8, ref sheet),
                            view = readValue(row, 9, ref sheet),
                            Keyword = readValue(row, 10, ref sheet),
                            legal_name = readValue(row, 11, ref sheet),
                            phone_mob = readValue(row, 12, ref sheet),
                            phone_city = readValue(row, 13, ref sheet),
                            email = readValue(row, 14, ref sheet),
                            Facebook_url = readValue(row, 15, ref sheet),
                            url_company = readValue(row, 16, ref sheet),
                            type_company = readValue(row, 17, ref sheet),
                            number_product_on_site = readValue(row, 18, ref sheet),
                            Availability_list_products_with_prices_on_site = readValue(row, 19, ref sheet),
                            region = readValue(row, 20, ref sheet),
                            address = readValue(row, 21, ref sheet),
                            Rating_of_trust_points = readValue(row, 22, ref sheet),
                            description = readValue(row, 23, ref sheet),
                            short_description_about_company = readValue(row, 24, ref sheet),
                            Additional_Information = readValue(row, 25, ref sheet),
                            Activities = readValue(row, 26, ref sheet),
                            company_news = readValue(row, 27, ref sheet),
                            Note = readValue(row, 28, ref sheet)
                        };

                        companys.Add(c);
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine("[ERROR] {0}", e);
                }
            }

            return companys;
        }

        public static void WriteXslx(List<Company> listInfo, string @path)
        {
            FileInfo newFile = new FileInfo(path);

            ExcelPackage excel = new ExcelPackage(newFile);

            if (excel.Workbook.Worksheets.Count == 0) excel.Workbook.Worksheets.Add("Content");
            var sheet = excel.Workbook.Worksheets.First();

            int row = 0;

            if (!newFile.Exists)
            {
                sheet.Cells[1, 1].Value = "Страна";

                sheet.Cells[1, 2].Value = "Источник";
                sheet.Cells[1, 3].Value = "Раздел в источнике";
                sheet.Cells[1, 4].Value = "Ссылка на карточку в каталоге";
                sheet.Cells[1, 5].Value = "Дата объявления";

                sheet.Cells[1, 6].Value = "Категория";

                sheet.Cells[1, 7].Value = "Юридическое название";

                sheet.Cells[1, 8].Value = "Моб. телефон";
                sheet.Cells[1, 9].Value = "Городской телефон";
                sheet.Cells[1, 10].Value = "Почта";
                sheet.Cells[1, 11].Value = "Facebook";
                sheet.Cells[1, 12].Value = "Сайт компании";

                sheet.Cells[1, 13].Value = "Регион";
                sheet.Cells[1, 14].Value = "Адрес";

                sheet.Cells[1, 15].Value = "О компании";

                row = 2;
            }
            else
            {
                row = sheet.Dimension.End.Row + 1;
            }


            listInfo.ForEach(company =>
            {
                try
                {
                    sheet.Cells[row, 1].Value = company.Country;

                    sheet.Cells[row, 2].Value = company.source;
                    sheet.Cells[row, 3].Value = company.section_in_source;
                    sheet.Cells[row, 4].Value = company.link_to_car_in_directory;
                    sheet.Cells[row, 5].Value = company.ad_date;

                    sheet.Cells[row, 6].Value = company.category;

                    sheet.Cells[row, 7].Value = company.legal_name;

                    sheet.Cells[row, 8].Value = company.phone_mob;
                    sheet.Cells[row, 9].Value = company.phone_city;
                    sheet.Cells[row, 10].Value = company.email;
                    sheet.Cells[row, 11].Value = company.Facebook_url;
                    sheet.Cells[row, 12].Value = company.url_company;

                    sheet.Cells[row, 13].Value = company.region;
                    sheet.Cells[row, 14].Value = company.address;

                    sheet.Cells[row, 15].Value = company.description;
                }
                catch (NullReferenceException) { }    

                row++;
            });

            Console.WriteLine("В файл сохранена компания №: {0}", row - 1);

            excel.Save();
        }

        public static void WriteXslx(Company company, string @path)
        {
            FileInfo newFile = new FileInfo(path);

            ExcelPackage excel = new ExcelPackage(newFile);

            if (excel.Workbook.Worksheets.Count == 0) excel.Workbook.Worksheets.Add("Content");

            var sheet = excel.Workbook.Worksheets.First();

            int row = 0;

            if (!newFile.Exists)
            {
                sheet.Cells[1, 1].Value = "Страна";

                sheet.Cells[1, 2].Value = "Источник";
                sheet.Cells[1, 3].Value = "Раздел в источнике";
                sheet.Cells[1, 4].Value = "Ссылка на карточку в каталоге";
                sheet.Cells[1, 5].Value = "Дата объявления";

                sheet.Cells[1, 6].Value = "Категория";

                sheet.Cells[1, 7].Value = "Юридическое название";

                sheet.Cells[1, 8].Value = "Моб. телефон";
                sheet.Cells[1, 9].Value = "Городской телефон";
                sheet.Cells[1, 10].Value = "Почта";
                sheet.Cells[1, 11].Value = "Facebook";
                sheet.Cells[1, 12].Value = "Сайт компании";

                sheet.Cells[1, 13].Value = "Регион";
                sheet.Cells[1, 14].Value = "Адрес";

                sheet.Cells[1, 15].Value = "О компании";
                row = 2;
            }
            else
            {
                row = sheet.Dimension.End.Row + 1;
            }

            try
            {
                sheet.Cells[row, 1].Value = company.Country;

                sheet.Cells[row, 2].Value = company.source;
                sheet.Cells[row, 3].Value = company.section_in_source;
                sheet.Cells[row, 4].Value = company.link_to_car_in_directory;
                sheet.Cells[row, 5].Value = company.ad_date;

                sheet.Cells[row, 6].Value = company.category;

                sheet.Cells[row, 7].Value = company.legal_name;

                sheet.Cells[row, 8].Value = company.phone_mob;
                sheet.Cells[row, 9].Value = company.phone_city;
                sheet.Cells[row, 10].Value = company.email;
                sheet.Cells[row, 11].Value = company.Facebook_url;
                sheet.Cells[row, 12].Value = company.url_company;

                sheet.Cells[row, 13].Value = company.region;
                sheet.Cells[row, 14].Value = company.address;

                sheet.Cells[row, 15].Value = company.description;
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine(e);
            }

            Console.WriteLine("В файл сохранена компания №: {0}", row - 1);

            excel.Save();
        }

        public static void writeToDb(List<Company> companys)
        {
            List<Company> error_list = new List<Company>();
            List<Company> company_set = new HashSet<Company>(companys).ToList();
            Console.WriteLine("Входящий список: {0}", companys.Count);
            Console.WriteLine("Сжатый список: {0}", company_set.Count);

            m_connection.Open();

            m_transaction = m_connection.BeginTransaction();

            company_set.ForEach(company => {
                writeToDb(company, ref error_list ,true);
                //Console.WriteLine("Загружено в базу {0} из {1}", ++current_count, all_count);
            });

            m_transaction.Commit();

            m_connection.Close();

            WriteXslx(error_list, "error_list.xlsx");

        }

        public static void writeToDb(Company company, ref List <Company> error_list, bool from_list = false)
        {
            try
            {
                if (!from_list) m_connection.OpenAsync();

                m_query = m_connection.CreateCommand();
                if (from_list) m_query.Transaction = m_transaction;
                m_query.CommandText = QUERY_INSERT;

                m_query.Parameters.AddWithValue("@country", company.Country);
                m_query.Parameters.AddWithValue("@source", company.source);
                m_query.Parameters.AddWithValue("@source_dir", company.section_in_source);
                m_query.Parameters.AddWithValue("@url_source_dir", company.link_to_car_in_directory);
                m_query.Parameters.AddWithValue("@ad_date", company.ad_date);
                m_query.Parameters.AddWithValue("@category", company.category);
                m_query.Parameters.AddWithValue("@group", company.group);
                m_query.Parameters.AddWithValue("@type", company.view);
                m_query.Parameters.AddWithValue("@keyword", company.Keyword);
                m_query.Parameters.AddWithValue("@name_legal", company.legal_name);
                m_query.Parameters.AddWithValue("@phone_mobile", company.phone_mob);
                m_query.Parameters.AddWithValue("@phone_city", company.phone_city);
                m_query.Parameters.AddWithValue("@email", company.email);
                m_query.Parameters.AddWithValue("@url_facebook", company.Facebook_url);
                m_query.Parameters.AddWithValue("@url_site", company.url_company);
                m_query.Parameters.AddWithValue("@type_company", company.type_company);
                m_query.Parameters.AddWithValue("@product_count", company.number_product_on_site);
                m_query.Parameters.AddWithValue("@has_price_list", company.Availability_list_products_with_prices_on_site);
                m_query.Parameters.AddWithValue("@region", company.region);
                m_query.Parameters.AddWithValue("@address", company.address);
                m_query.Parameters.AddWithValue("@rating", company.Rating_of_trust_points);
                m_query.Parameters.AddWithValue("@desc_full", company.description);
                m_query.Parameters.AddWithValue("@desc_short", company.short_description_about_company);
                m_query.Parameters.AddWithValue("@desc_add", company.Additional_Information);
                m_query.Parameters.AddWithValue("@type_activitie", company.Activities);
                m_query.Parameters.AddWithValue("@news", company.company_news);
                m_query.Parameters.AddWithValue("@note", company.Note);

                m_query.Prepare();

                m_query.ExecuteNonQuery();
                // store

                if (!from_list) m_connection.CloseAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] Ошибка при записи в базу данных: {0}\n{1}", company.legal_name, e.Message);
                error_list.Add(company);
            }
        }

        public static string readValue(int row, int column, ref ExcelWorksheet sheet)
        {
            string value;

            try
            {
                value = sheet.GetValue(row, column).ToString().Trim();
            }
            catch
            {
                value = string.Empty;
            }

            return value;
        }

        public static void repairList(string @path)
        {
            List<Company> list = ReadXlsx(path);
            list.ForEach(c => { c.do_filter(); });
            WriteXslx(list, "repair-" + path);
        }

        public static void mergeList(string @path, bool pulscen = false)
        {
            List<Company> list = new List<Company>();
            HashSet <Company> new_list = new HashSet<Company>();

            List <string> files = Directory.GetFiles(path).ToList();
            files.ForEach(file =>
            {
                Console.WriteLine("Read file...");
                List<Company> companys = ReadXlsx(file);
                list.AddRange(companys);
            });

            Console.WriteLine("Filter...");

            list.ForEach(c => {

                //c.do_filter(true);

                //if (pulscen && c.url_company.Contains("goto_online_store?")) return;

                bool empty_phone = string.IsNullOrWhiteSpace(c.phone_city) && string.IsNullOrWhiteSpace(c.phone_mob);
                bool empty_email = string.IsNullOrWhiteSpace(c.email);
                bool empty_site = string.IsNullOrWhiteSpace(c.url_company);

                if (empty_phone && empty_email && empty_site) return;
                

                c.do_post_filter();

                empty_phone = string.IsNullOrWhiteSpace(c.phone_city) && string.IsNullOrWhiteSpace(c.phone_mob);
                if (empty_phone && empty_email && empty_site) return;

                new_list.Add(c);
            });

            list.Clear();

            WriteXslx(new_list.ToList(), "merged.xlsx");
            new_list.Clear();
        }

        public static void fromPathToDb(string @path) {
			List<Company> list = new List<Company>();
			List<string> files = Directory.GetFiles(path).ToList();
			files.ForEach(file =>
			{
                Console.WriteLine("Read: {0}", path);
				List<Company> companys = ReadXlsx(file);
				list.AddRange(companys);
			});

            writeToDb(list);
        }


        private static string toUtf8(string input)
        {
            byte[] bytes = Encoding.Default.GetBytes(input);
            return Encoding.UTF8.GetString(bytes);
        }
    }
}
